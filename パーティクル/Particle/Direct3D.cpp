#include <assert.h>
#include "Direct3D.h"
#include "Global.h"
#include "resource.h"
#include <CommCtrl.h>

//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//Direct3Dを使うためのコンポーネント
LPDIRECT3D9 pD3d = nullptr;
LPDIRECT3DDEVICE9 Direct3D::pDevice = nullptr;

//カメラの位置
D3DXVECTOR3 cameraPos;
//カメラが向いている方向
D3DXVECTOR3 cameraTarget;

//背景の色
D3DXCOLOR backColor;

void Direct3D::Initialize(HWND hWnd)
{
	//マシンの描画能力などの情報が入ったインスタンスを作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);
	assert(pD3d != nullptr);

	//デバイスの設定をする
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = TRUE;						//ウィンドウモードかフルスクリーンモードか 今回のプログラムはフルスクリーンのように見えるがフルスクリーンではない
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;	//フロントバッファとバックバッファの切り替え方法 ディスプレイドライバが自動判断
	d3dpp.EnableAutoDepthStencil = TRUE;		//zバッファを使用する
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;	//zバッファのフォーマット

	//設定した変数を元にデバイスを作成
	pD3d->CreateDevice(
		D3DADAPTER_DEFAULT,					 //ディスプレイの番号 デフォルト
		D3DDEVTYPE_HAL,						 //描画の処理をハードウェアで行うかソフトウェアソフトウェアで行うか ハードウェアで行う
		hWnd,								 //描画を行うウィンドウハンドル
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //頂点処理をハードウェアで行うかソフトウェアで行うか ハードウェアで行う
		&d3dpp,								 //デバイスの設定が入った変数
		&pDevice							 //デバイスのインスタンスのポインタを入れる変数
	);
	assert(pDevice != nullptr);

	//ポイントスプライトの減衰率
	float scaleA = 0;
	float scaleB = 0;
	float scaleC = 1.0f;

	//ポイントスプライトを使うための設定
	pDevice->SetRenderState(D3DRS_POINTSPRITEENABLE, TRUE);			//ポイントスプライトとして扱う テクスチャが張れるようになる
	pDevice->SetRenderState(D3DRS_POINTSCALEENABLE, TRUE);			//ポイントスプライトの大きさを変えられるようにする
	pDevice->SetRenderState(D3DRS_POINTSCALE_A, *((DWORD*)&scaleA));//カメラとの距離によって大きさを変えるための計算に使う値
	pDevice->SetRenderState(D3DRS_POINTSCALE_B, *((DWORD*)&scaleB));//カメラとの距離によって大きさを変えるための計算に使う値
	pDevice->SetRenderState(D3DRS_POINTSCALE_C, *((DWORD*)&scaleC));//カメラとの距離によって大きさを変えるための計算に使う値

	//ライティング
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	//zバッファ
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

	//テクスチャと頂点のアルファ値を掛け合わせて使う
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	//アルファブレンド
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//初期設定では重なった部分の色を加算合成
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	//プロジェクション行列を作るための行列 (行列を保存するための変数, 画角, アスペクト比ウィンドウの比率, 描画し始める位置を面にしないといけないため少し離す, どこまで表示するか)
	D3DXMATRIX projection;
	D3DXMatrixPerspectiveFovLH(&projection, D3DXToRadian(60), (float)GetSystemMetrics(SM_CXSCREEN) / GetSystemMetrics(SM_CYSCREEN), 0.5f, 2000.0f);
	pDevice->SetTransform(D3DTS_PROJECTION, &projection);
}

void Direct3D::BeginDraw()
{
	//画面のクリア 描画する前に必ず呼ぶ
	pDevice->Clear(
		0,									//塗りつぶす範囲の個数
		NULL,								//塗りつぶす範囲が入った配列
		D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,	//塗りつぶすバッファ バックバッファとzバッファ
		D3DCOLOR_XRGB((int)backColor.r, (int)backColor.g, (int)backColor.b),//塗りつぶす色
		1.0f,								//zバッファを使っていた場合クリアする深度 最大の1.0fを指定
		0									//ステンシルバッファを使っていた場合クリアする色
	);

	//描画の開始 正確にはバックバッファに書き込み
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画の終了
	pDevice->EndScene();

	//バックバッファとフトントバッファをスワップ
	pDevice->Present(NULL, NULL, NULL, NULL);
}

void Direct3D::Release()
{
	pDevice->Release();
	pD3d->Release();
}

void Direct3D::ViewTransform(D3DXVECTOR3 position, D3DXVECTOR3 target, D3DXVECTOR3 rotate)
{
	cameraPos = position;
	cameraTarget = target;

	//カメラの回転の設定
	D3DXMATRIX m, mX, mY, mZ;
	D3DXMatrixRotationX(&mX, D3DXToRadian(rotate.x));
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate.y));
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotate.z));
	m = mX * mY * mZ;
	D3DXVECTOR3 pUp = D3DXVECTOR3(0, 1, 0);
	D3DXVec3TransformCoord(&pUp, &pUp, &m);

	D3DXMATRIX view;
	//ビュー行列を作るための関数 (計算結果を入れる変数, &位置, どの位置を見るか, カメラそのものを傾ける)
	D3DXMatrixLookAtLH(&view, &position, &target, &pUp);
	//どの座標として使うかを指定
	pDevice->SetTransform(D3DTS_VIEW, &view);
}

D3DXVECTOR3 Direct3D::GetCameraPosition()
{
	return cameraPos;
}

D3DXVECTOR3 Direct3D::GetCameraTarget()
{
	return cameraTarget;
}

void Direct3D::CommandProcess(HWND hDlg, WPARAM wp)
{
	//エラー判定
	BOOL error;

	switch (LOWORD(wp))
	{
	case IDC_COLOR_RADIO:
		//カラーで設定された色を使う
		pDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
		pDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
		break;

	case IDC_TEXTURE_RADIO:
		//テクスチャの色を使うよう
		pDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		pDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		break;

	case IDC_BLEND_RADIO:
		//カラーとテクスチャの色を混ぜて使う
		pDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		pDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
		pDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
		break;

	case IDC_INVSRC_RADIO:
		//重なった部分を加算
		pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		break;

	case IDC_ONE_RADIO:
		//重なった部分を減算
		pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		break;

	case IDC_ZBUFFER_CHECK:
		//zバッファ適応
		if (BST_CHECKED == SendMessage(GetDlgItem(hDlg, IDC_ZBUFFER_CHECK), BM_GETCHECK, 0, 0))
		{
			pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
		}
		else
		{
			pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
		}
		break;

	case IDC_BACK_COLOR_R_EDIT:
		//背景の色 赤
		backColor.r = SendMessage(GetDlgItem(hDlg, IDC_BACK_COLOR_R_SPIN), UDM_GETPOS32, 0, (LPARAM)&error);
		if (error != 0)
		{
			backColor.r = 0;
		}
		break;

	case IDC_BACK_COLOR_G_EDIT:
		//背景の色 緑
		backColor.g = SendMessage(GetDlgItem(hDlg, IDC_BACK_COLOR_G_SPIN), UDM_GETPOS32, 0, (LPARAM)&error);
		if (error != 0)
		{
			backColor.g = 0;
		}
		break;

	case IDC_BACK_COLOR_B_EDIT:
		//背景の色 青
		backColor.b = SendMessage(GetDlgItem(hDlg, IDC_BACK_COLOR_B_SPIN), UDM_GETPOS32, 0, (LPARAM)&error);
		if (error != 0)
		{
			backColor.b = 0;
		}
		break;
	}
}
