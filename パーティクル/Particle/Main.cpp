#include <Windows.h>
#include <time.h>
#ifdef _DEBUG
#include <crtdbg.h>
#endif

#include <conio.h>
#include <CommCtrl.h>

#include "Direct3D.h"
#include "Particle.h"
#include "Global.h"
#include "resource.h"


//ウィンドウプロシージャの前方宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
//ダイアログプロシージャ
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);
//ダイアログの初期化
void InitDialog(HWND hDlg);
//スピンコントロールの設定
//引数 ダイアログのウィンドウハンドル, 設定したいスピンコントロールのID, 最小値, 最大値, 初期値
void SetSpinControl(HWND hDlg, int id, int min, int max, int init);

//パーティクルを生成するためのインスタンス
Particle* pParticle = new Particle();

//ウィンドウハンドル
HWND hWnd;

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	//メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス 作るウィンドウの設定をする
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);             //この構造体のサイズ
	wc.hInstance = hInstance;                   //インスタンスハンドルを指定
	wc.lpszClassName = "particle";				//ウィンドウクラス名
	wc.lpfnWndProc = WndProc;                   //ウィンドウプロシージャ(プロトタイプ宣言にある)
	wc.style = CS_VREDRAW | CS_HREDRAW;         //縦と横のサイズが変わったときウィンドウを再描画する
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION); //ショートカットを作ったときなどに使われるアイコン 標準のものを設定
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);   //タイトルバーの左端にあるような小さいアイコン 標準のものを設定
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);   //マウスカーソル 矢印
	wc.lpszMenuName = NULL;                     //メニュー なし
	wc.cbClsExtra = 0;							//補助容量 なし
	wc.cbWndExtra = 0;							//補助容量 なし
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); //背景 白

	RegisterClassEx(&wc); //クラスを登録

	//ウィンドウの大きさ スクリーンの大きさを取得して設定
	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//ウィンドウを作成
	hWnd = CreateWindow(
		"particle",			 //ウィンドウクラス名
		"particle",			//タイトルバーに表示する内容
		WS_POPUP,			//表示スタイル ポップアップにする
		0,					//表示位置左
		0,					//表示位置上
		width,				//ウィンドウ幅 
		height,				//ウィンドウ高さ
		NULL,				//親ウインドウ なし
		NULL,				//メニュー なし
		hInstance,			//インスタンスハンドル
		NULL				//WM_CREATEされたときlParamにCREATESTRUCT構造体へのポインタが入っておりそのメンバのlpCreateParamsに入る値 なし
	);

	ShowWindow(hWnd, nCmdShow);

	//ランダムの初期化
	srand(time(NULL));

	//direct3Dの初期化
	Direct3D::Initialize(hWnd);

	//カメラの設定
	Direct3D::ViewTransform(D3DXVECTOR3(0, 0, -300), D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 1, 0));

	//パーティクルの頂点バッファ確保
	pParticle->CreateVertexBuffer();

	//ダイアログの生成
	CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, (DLGPROC)DialogProc);

	//メッセージループ
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		//メッセージなし
		else
		{
			pParticle->Update();

			Direct3D::BeginDraw();
			pParticle->Draw();
			Direct3D::EndDraw();
		}
	}
	
	SAFE_DELETE(pParticle);
	Direct3D::Release();

	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0); //プログラム終了
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg)
	{
	case WM_INITDIALOG:
		InitDialog(hDlg);
		return TRUE;

	case WM_COMMAND:
		pParticle->CommandProcess(hDlg, wp);
		Direct3D::CommandProcess(hDlg, wp);
		return TRUE;

	case WM_CLOSE:
		DestroyWindow(hWnd);
		return TRUE;
	}
	//何もしていないときFALSE
	return FALSE;
}

void InitDialog(HWND hDlg)
{
	//スピンコントロールの初期化
	auto InitSpinControl = [hDlg](int id, int min, int max, int init)
	{
		SendMessage(GetDlgItem(hDlg, id), UDM_SETRANGE32, (WPARAM)min, (LPARAM)max);
		SendMessage(GetDlgItem(hDlg, id), UDM_SETPOS, 0, (LPARAM)init);
	};

	//カラーで指定した色を使うように設定
	SendMessage(GetDlgItem(hDlg, IDC_COLOR_RADIO), BM_SETCHECK, BST_CHECKED, 0);

	//デフォルトで使用出来るテクスチャを設定
	SendMessage(GetDlgItem(hDlg, IDC_TEXTURE_COMBO), CB_INSERTSTRING, 0, (LPARAM)"なし");
	SendMessage(GetDlgItem(hDlg, IDC_TEXTURE_COMBO), CB_INSERTSTRING, 1, (LPARAM)"円");
	//テクスチャなし
	SendMessage(GetDlgItem(hDlg, IDC_TEXTURE_COMBO), CB_SETCURSEL, 0, 0);

	//重なるとアルファ値をもとに色を加算する
	SendMessage(GetDlgItem(hDlg, IDC_INVSRC_RADIO), BM_SETCHECK, BST_CHECKED, 0);

	//zバッファを適用する
	//SendMessage(GetDlgItem(hDlg, IDC_ZBUFFER_CHECK), BM_SETCHECK, BST_CHECKED, 0);
	
	
	//基準の色
	InitSpinControl(IDC_BASE_COLOR_A_SPIN, 0, 255, 0);
	InitSpinControl(IDC_BASE_COLOR_R_SPIN, 0, 255, 0);
	InitSpinControl(IDC_BASE_COLOR_G_SPIN, 0, 255, 0);
	InitSpinControl(IDC_BASE_COLOR_B_SPIN, 0, 255, 0);

	//ランダムの色
	InitSpinControl(IDC_RANDOM_COLOR_A_SPIN, 0, 255, 255);
	InitSpinControl(IDC_RANDOM_COLOR_R_SPIN, 0, 255, 255);
	InitSpinControl(IDC_RANDOM_COLOR_G_SPIN, 0, 255, 255);
	InitSpinControl(IDC_RANDOM_COLOR_B_SPIN, 0, 255, 255);

	//背景の色
	InitSpinControl(IDC_BACK_COLOR_R_SPIN, 0, 255, 0);
	InitSpinControl(IDC_BACK_COLOR_G_SPIN, 0, 255, 0);
	InitSpinControl(IDC_BACK_COLOR_B_SPIN, 0, 255, 0);

	//画面にだす最大個数
	InitSpinControl(IDC_MAX_COUNT_SPIN, 0, pParticle->GetMaxCapacity(), 0);

	//1フレームに出すパーティクルの個数
	InitSpinControl(IDC_ADD_DRAW_SPIN, 0, pParticle->GetMaxCapacity(), 0);

	//基準サイズ
	InitSpinControl(IDC_BASE_SIZE_SPIN, 0, 100, 1);

	//ランダムサイズ
	InitSpinControl(IDC_RANDOM_SIZE_SPIN, 0, 100, 0);

	//基準位置(zのみ)
	InitSpinControl(IDC_BASE_POS_Z_SPIN, -500, 500, 0);

	//ランダム位置
	InitSpinControl(IDC_RANDOM_POS_X_SPIN, 0, 500, 0);
	InitSpinControl(IDC_RANDOM_POS_Y_SPIN, 0, 500, 0);
	InitSpinControl(IDC_RANDOM_POS_Z_SPIN, 0, 500, 0);

	//スピード
	InitSpinControl(IDC_SPEED_SPIN, 0, 20, 1);

	//公転スピード
	InitSpinControl(IDC_REVOLUTION_SPEED_SPIN, 0, 20, 0);
	
	//動きの種類
	SendMessage(GetDlgItem(hDlg, IDC_ATTRACT_RADIO), BM_SETCHECK, BST_CHECKED, 0);

	//公転の動き
	//固定値
	InitSpinControl(IDC_REVORUTION_X_SPIN, -100, 100, 1);
	InitSpinControl(IDC_REVORUTION_Y_SPIN, -100, 100, 0);
	InitSpinControl(IDC_REVORUTION_Z_SPIN, -100, 100, 0);

	//距離によってアルファ値を変える
	//SendMessage(GetDlgItem(hDlg, IDC_DISTANCE_ALPHA_CHECK), BM_SETCHECK, BST_CHECKED, 0);

	//ループしない
	//SendMessage(GetDlgItem(hDlg, IDC_LOOP_CHECK), BM_SETCHECK, BST_CHECKED, 0);

	//ループ範囲
	InitSpinControl(IDC_LOOP_RANGE_SPIN, 1, 500, 200);

	//重力
	InitSpinControl(IDC_GRAVITY_SPIN, 0, 10, 1);

	//回転を使う
	//SendMessage(GetDlgItem(hDlg, IDC_REVORUTION_CHECK), BM_SETCHECK, BST_CHECKED, 0);

	//拡散度
	InitSpinControl(IDC_DIFFUSIVITY_SPIN, 1, 360, 360);

	//何フレームでループするか
	InitSpinControl(IDC_LOOP_COUNT_SPIN, 1, 5000, 60);
}