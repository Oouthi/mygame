#include <assert.h>
#include <stack>
#define _USE_MATH_DEFINES
#include <math.h>
#include "Particle.h"
#include "Direct3D.h"
#include "Global.h"
#include "resource.h"
#include <CommCtrl.h>

//パーティクルの最大数
const int MAX_CAPACITY = 500000;

//既存テクスチャ
const LPCSTR textureFileName[TEXTURE_ENUM_MAX]
{
	"Texture/circle.png",
};

Particle::Particle() :
	pVertexBuffer_(NULL), pTexture_(NULL), position_(D3DXVECTOR3(0, 0, 0)), randomPosition_(D3DXVECTOR3(0, 0, 0)),
	basePosition_(D3DXVECTOR3(0, 0, 0)), axis_(D3DXVECTOR3(0, 0, 0)),
	randomColor_(D3DXCOLOR(255, 255, 255, 255)), baseColor_(D3DXCOLOR(0, 0, 0, 0)),
	maxDrawCount_(0), drawCount_(0), addDrawCount_(0), baseSize_(1), diffusivity_(360), loopCount_(0),
	randomSize_(0), speed_(0), revolutionSpeed_(0), attractGenerateRange_(0), gravity_(1),
	isSizeChange_(true), isColorChange_(true), isSort_(true), isDistanceAlpha_(true), isRevolution_(false),
	isLoop_(false), isAxisChange_(true), moveTypeState_(ATTRACT)
{
	//パーティクルの容量生成
	pVertexData_ = new ParticleVertexData[MAX_CAPACITY];

	//関数ポインタに関数をセット
	moveFuncArray_[ATTRACT] = &Particle::Attract;
	moveFuncArray_[PULL_APART] = &Particle::PullApart;
}

Particle::~Particle()
{
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_RELEASE(pTexture_);
	SAFE_DELETE_ARRAY(pVertexData_);
}

void Particle::Update()
{
	UpdatePosition();
	
	AddParticle();

	UpdateVertexData();

	if (isSort_)
	{
		DistanceQuickSort();
	}

	//頂点情報を書き込む
	//書き込むアドレスのポインタを入れる変数
	ParticleVertexData *vCopy;
	//メモリの書き込みをする際にほかのプログラムに邪魔されないようにメモリをロックする
	//ロック位置, ロックするサイズ 全体をロックする場合0, ロックされたメモリ範囲の先頭アドレスへのポインタを入れる変数, オプション
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	//頂点情報が入った配列をそのままコピー
	memcpy(vCopy, pVertexData_, drawCount_ * sizeof(ParticleVertexData));
	//解放処理
	pVertexBuffer_->Unlock();
}

void Particle::UpdateVertexData()
{
	//ループするとき
	if (isLoop_)
	{
		for (int i = 0; i < drawCount_; i++)
		{
			pVertexData_[i].loopCount++;

			if (loopCount_ <= pVertexData_[i].loopCount)
			{
				//表示する範囲の最後の要素と生成されてからのカウントがloopCount_に達したパーティクルを交換して
				//表示する範囲をデクリメントする
				pVertexData_[i] = pVertexData_[drawCount_ - 1];
				drawCount_--;
				i--;
				continue;
			}
		}
	}

	//色変更
	if (isColorChange_)
	{
		ChangeColor(0, drawCount_);
	}
	isColorChange_ = false;

	//サイズ変更
	if (isSizeChange_)
	{
		ChangeSize(0, drawCount_);
	}
	isSizeChange_ = false;

	//動き
	(this->*moveFuncArray_[moveTypeState_])();

	//公転
	if (isRevolution_)
	{
		for (int i = 0; i < drawCount_; i++)
		{
			//パーティクルから中心点までの距離
			int distance = D3DXVec3Length(&(position_ - pVertexData_[i].position));

			//xyzそれぞれの回転角度
			float rotateAngleX, rotateAngleY, rotateAngleZ;
			if (distance == 0)
			{
				//パーティクルから中心点までの距離が0ならすべての角度は0になる
				rotateAngleX = 0;
				rotateAngleY = 0;
				rotateAngleZ = 0;
			}
			else
			{
				//xyzそれぞれの角度 = xyzそれぞれの弧の中心角 = 
				//(弧の長さ * zyxそれぞれの割合 * 180) / (パーティクルから中心点までの距離(半径) * 円周率)
				rotateAngleX = (revolutionSpeed_ * axis_.x * 180) / (distance * M_PI);
				rotateAngleY = (revolutionSpeed_ * axis_.y * 180) / (distance * M_PI);
				rotateAngleZ = (revolutionSpeed_ * axis_.z * 180) / (distance * M_PI);
			}

			//回転行列
			D3DXMATRIX m, mX, mY, mZ;
			//X軸回転
			D3DXMatrixRotationX(&mX, D3DXToRadian(rotateAngleX));
			//Y軸回転
			D3DXMatrixRotationY(&mY, D3DXToRadian(rotateAngleY));
			//Z軸回転
			D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotateAngleZ));

			m = mX * mY * mZ;
			//この回転行列はワールド座標(0,0,0)を中心に回転させる
			//しかしパーティクルは中心点(position_)を軸に発生・移動をさせているためそのままでは正しく回転できないので
			//パーティクルから中心点を引いてワールド座標(0,0,0)を軸にしてから回転させ、そのあと中心点を足して元に戻す
			D3DXVec3TransformCoord(&pVertexData_[i].position, &(pVertexData_[i].position + (position_ * -1)), &m);
			pVertexData_[i].position += position_;
		}
	}

	//生成されてからの時間によってアルファ値を変える
	//ループの時のみ有効
	if (isDistanceAlpha_)
	{
		for (int i = 0; i < drawCount_; i++)
		{
			unsigned char alpha = 255 - 255 * pVertexData_[i].loopCount / loopCount_;
			//D3DCOLOR_ARGBで色を決めていたためARGBの順番でデータが入っていると思っていたが逆の順番で入っていた
			//リトルエンディアンだからか？
			pVertexData_[i].color = D3DCOLOR_ARGB(
				alpha,
				((unsigned char*)&pVertexData_[i].color)[2],
				((unsigned char*)&pVertexData_[i].color)[1],
				((unsigned char*)&pVertexData_[i].color)[0]);
		}
	}
}

void Particle::DistanceQuickSort()
{
	//スタックオーバーフローを起こさないように再帰しないでソートする

	//ソートしたい範囲を入れる構造体
	struct sortRange 
	{
		int start;
		int end;
	};

	//分割しながらソートしていくためスタックに溜める
	std::stack<sortRange> sortRangeStack;
	sortRangeStack.push({0, drawCount_ - 1 });

	//drawCount_が0の場合ソートしない
	if (sortRangeStack.top().start > sortRangeStack.top().end)
	{
		return;
	}

	while (!sortRangeStack.empty())
	{
		//ソートするために調べる配列番号
		int left = sortRangeStack.top().start;
		int right = sortRangeStack.top().end;

		//カメラの向き
		D3DXVECTOR3 cameraDirection;
		D3DXVec3Normalize(&cameraDirection, &(Direct3D::GetCameraTarget() - Direct3D::GetCameraPosition()));

		//ソートしたい範囲の先頭と末尾の要素のビュー行列の視線ベクトルに投影した場合の距離を足して2で割ったものをピポットにする
		float pivot = (D3DXVec3Dot(&(pVertexData_[sortRangeStack.top().start].position - Direct3D::GetCameraPosition()), &cameraDirection) + 
					  D3DXVec3Dot(&(pVertexData_[sortRangeStack.top().end].position - Direct3D::GetCameraPosition()), &cameraDirection)) / 2;
		
		while (left <= right)
		{
			//ビュー行列の視線ベクトルに投影した場合の距離がピポットより小さい要素が見つかるまでループ
			while (D3DXVec3Dot(&(pVertexData_[left].position - Direct3D::GetCameraPosition()), &cameraDirection) > pivot)
			{
				++left;
			}
			//ビュー行列の視線ベクトルに投影した場合の距離がピポットより大きい要素が見つかるまでループ
			while (D3DXVec3Dot(&(pVertexData_[right].position - Direct3D::GetCameraPosition()), &cameraDirection) < pivot)
			{
				--right;
			}

			if (left <= right)
			{
				//leftとrightの値を交換
				ParticleVertexData swapData = pVertexData_[left];
				pVertexData_[left] = pVertexData_[right];
				pVertexData_[right] = swapData;
				++left;
				--right;
			}
		}

		//スタックに要素を足す
		int start = sortRangeStack.top().start;
		int end = sortRangeStack.top().end;
		sortRangeStack.pop();

		if (start < right) 
		{
			//rightは最後必ずleftを超えるので次に見る範囲はstartからrightまで
			sortRangeStack.push({ start, right });
		}
		if (left < end) {
			//leftは最後必ずrightを超えるので次に見る範囲はleftからstartまで
			sortRangeStack.push({ left, end });
		}
	}
}

void Particle::CreateVertexBuffer()
{
	Direct3D::pDevice->CreateVertexBuffer(
		MAX_CAPACITY * sizeof(ParticleVertexData),	//サイズ
		D3DUSAGE_WRITEONLY,							//バッファをどのように扱うか 書き込み専用
		D3DFVF_XYZ | D3DFVF_PSIZE | D3DFVF_DIFFUSE,	//頂点にどのような情報があるか 位置とサイズと色
		D3DPOOL_MANAGED,							//頂点バッファをどのメモリに置くか 適当に管理してもらう
		&pVertexBuffer_,							//頂点情報にアクセスするためのポインタ変数
		NULL										//現在使用されていないのでNULL
	);
	assert(pVertexBuffer_ != nullptr);
}

void Particle::AddParticle()
{
	//追加するパーティクルの最後の要素番号
	int addEnd;

	if ((drawCount_ + addDrawCount_) < maxDrawCount_)
	{
		addEnd = drawCount_ + addDrawCount_;
	}
	else
	{
		addEnd = maxDrawCount_;
	}

	//追加する前に初期設定
	//色
	ChangeColor(drawCount_, addEnd);

	//サイズ
	ChangeSize(drawCount_, addEnd);

	//パーティクルの初期の距離
	//ひきつける動きをする場合はattractGenerateRange_分中心点から離す
	//引き離す動きをする場合はほぼ中心点と同じ場所に設定する
	int initPositionDistance[MOVE_TYPE_MAX];
	initPositionDistance[ATTRACT] = attractGenerateRange_;
	initPositionDistance[PULL_APART] = 1;

	//パーティクルの初期の進行方向係数
	int initDirCoefficient[MOVE_TYPE_MAX];
	initDirCoefficient[ATTRACT] = -1;
	initDirCoefficient[PULL_APART] = 1;

	//生成位置と方向
	for (int i = drawCount_; i < addEnd; i++)
	{
		//パーティクルの初期進行方向
		D3DXVECTOR3 dir;

		//あるベクトル(今回は(0,1,0))を軸にdiffusivity_の値分拡散させる
		dir = D3DXVECTOR3(0, 1, 0);
		D3DXMATRIX mX, mY, m;
		if (diffusivity_ == 0)
		{
			D3DXMatrixIdentity(&mX);
		}
		else
		{
			D3DXMatrixRotationX(&mX, D3DXToRadian(rand()% diffusivity_ - diffusivity_ / 2));
		}
		D3DXMatrixRotationY(&mY, D3DXToRadian(rand() % 360));
		D3DXVec3TransformCoord(&dir, &dir, &(mX * mY));

		//最初に設定したベクトル(0,1,0)とaxis_との角度を求める
		float angle = acos(D3DXVec3Dot(&D3DXVECTOR3(0, 1, 0), &axis_));

		//最初に設定したベクトル(0,1,0)とaxis_との外積を計算して回転軸を求める
		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &D3DXVECTOR3(0, 1, 0), &axis_);

		//D3DXMatrixRotationAxisを使ってcrossを回転軸にangle度回転する行列を作る
		D3DXMatrixRotationAxis(&m, &cross, angle);

		//dirを回転させる
		D3DXVec3TransformCoord(&dir, &dir, &m);

		//dirにinitPositionDistanceを掛けて初期位置にする
		pVertexData_[i].position = dir * initPositionDistance[moveTypeState_] + position_;
		//dirにinitDirCoefficientをかけて初期進行方向にする
		pVertexData_[i].moveDir = dir * initDirCoefficient[moveTypeState_];

		//ループカウントをリセット
		pVertexData_[i].loopCount = 0;
	}

	//表示する個数を変更
	drawCount_ = addEnd;
}

void Particle::UpdatePosition()
{
	position_ = D3DXVECTOR3(
		basePosition_.x + rand() % (int)(randomPosition_.x + 1) - randomPosition_.x / 2,
		basePosition_.y + rand() % (int)(randomPosition_.y + 1) - randomPosition_.y / 2,
		basePosition_.z + rand() % (int)(randomPosition_.z + 1) - randomPosition_.z / 2);
}

void Particle::Attract()
{
	for (int i = 0; i < drawCount_; i++)
	{
		//パーティクルの位置から中心点に向かうベクトル
		D3DXVECTOR3 moveDir = position_ - pVertexData_[i].position;
		D3DXVec3Normalize(&moveDir, &moveDir);
		//gravity_に設定した値によって方向ベクトルを減衰
		moveDir /= (10 - gravity_ + 1);

		//今パーティクルに設定している方向を修正
		moveDir += pVertexData_[i].moveDir;
		D3DXVec3Normalize(&moveDir, &moveDir);
		pVertexData_[i].moveDir = moveDir;

		//修正した方向をもとに移動
		pVertexData_[i].position += pVertexData_[i].moveDir * speed_;
	}
}

void Particle::PullApart()
{
	for (int i = 0; i < drawCount_; i++)
	{
		//パーティクルを生成したときに決めたmoveDirの方向に飛んでいく
		pVertexData_[i].position += pVertexData_[i].moveDir * speed_;
	}
}

void Particle::ChangeColor(int start, int end)
{
	for (int i = start; i < end; i++)
	{
		pVertexData_[i].color = D3DCOLOR_ARGB(
			(int)(baseColor_.a + rand() % (int)(randomColor_.a + 1)),
			(int)(baseColor_.r + rand() % (int)(randomColor_.r + 1)),
			(int)(baseColor_.g + rand() % (int)(randomColor_.g + 1)),
			(int)(baseColor_.b + rand() % (int)(randomColor_.b + 1)));
	}
}

void Particle::ChangeSize(int start, int end)
{
	for (int i = start; i < end; i++)
	{
		//サイズ
		//サイズは1変わるだけでかなり変化があるので小数点以下もランダムにする
		if (randomSize_ > 0)
		{
			pVertexData_[i].size = (float)(baseSize_ + rand() % (randomSize_ + 1) + (float)rand() / RAND_MAX - 1);
		}
		else
		{
			pVertexData_[i].size = (float)baseSize_;
		}
	}
}

void Particle::CommandProcess(HWND hDlg, WPARAM wp)
{
	//スピンコントロールから値を取得する関数
	//戻り値 スピンコントロールから取得した値, スピンコントロールの値が範囲外だった場合0が返る
	//引数 ダイアログのウィンドウハンドル, スピンコントロールのスピンコントロールのID
	auto GetSpinControl = [hDlg](int id)->int
	{
		//範囲外の数値だった場合0を返す
		BOOL error;
		int number = SendMessage(GetDlgItem(hDlg, id), UDM_GETPOS32, 0, (LPARAM)&error);
		if (error != 0)
		{
			number = 0;
		}
		return number;
	};

	//すべてswitch一つで書きたかったが項目が30個程度あり見にくくなると思ったので
	//ダイアログのどこかに変更があったとき変更されていない項目もダイアログから値を取ってきて代入の処理が入る

	//zバッファを適用するか
	if (BST_CHECKED == SendMessage(GetDlgItem(hDlg, IDC_ZBUFFER_CHECK), BM_GETCHECK, 0, 0))
	{
		isSort_ = true;
	}
	else
	{
		isSort_ = false;
	}

	//使うテクスチャ
	if (LOWORD(wp) == IDC_TEXTURE_COMBO)
	{
		int textureNumber = SendMessage(GetDlgItem(hDlg, IDC_TEXTURE_COMBO), CB_GETCURSEL, 0, 0);
		if (textureNumber == 0)
		{
			SetTexture(nullptr);
		}
		else
		{
			SetTexture((TEXTURE_ENUM)(textureNumber - 1));
		}
	}

	//色
	baseColor_.a = GetSpinControl(IDC_BASE_COLOR_A_SPIN);
	baseColor_.r = GetSpinControl(IDC_BASE_COLOR_R_SPIN);
	baseColor_.g = GetSpinControl(IDC_BASE_COLOR_G_SPIN);
	baseColor_.b = GetSpinControl(IDC_BASE_COLOR_B_SPIN);
	//ランダムの色
	randomColor_.a = GetSpinControl(IDC_RANDOM_COLOR_A_SPIN);
	randomColor_.r = GetSpinControl(IDC_RANDOM_COLOR_R_SPIN);
	randomColor_.g = GetSpinControl(IDC_RANDOM_COLOR_G_SPIN);
	randomColor_.b = GetSpinControl(IDC_RANDOM_COLOR_B_SPIN);
	
	//基準値とランダムの値を足した値が255を超えないように設定する
	if (baseColor_.a + randomColor_.a >= 255)
	{
		randomColor_.a = 255 - baseColor_.a;
	}
	if (baseColor_.r + randomColor_.r >= 255)
	{
		randomColor_.r = 255 - baseColor_.r;
	}
	if (baseColor_.g + randomColor_.g >= 255)
	{
		randomColor_.g = 255 - baseColor_.g;
	}
	if (baseColor_.b + randomColor_.b >= 255)
	{
		randomColor_.b = 255 - baseColor_.b;
	}

	isColorChange_ = true;

	//最大数
	maxDrawCount_ = GetSpinControl(IDC_MAX_COUNT_SPIN);

	//1フレームに出す個数
	addDrawCount_ = GetSpinControl(IDC_ADD_DRAW_SPIN);

	//基準サイズ
	baseSize_ = GetSpinControl(IDC_BASE_SIZE_SPIN);

	//ランダムサイズ
	randomSize_ = GetSpinControl(IDC_RANDOM_SIZE_SPIN);

	isSizeChange_ = true;

	//位置
	//基準値(zだけ)
	basePosition_.z = GetSpinControl(IDC_BASE_POS_Z_SPIN);

	//ランダム値
	randomPosition_.x = GetSpinControl(IDC_RANDOM_POS_X_SPIN);
	randomPosition_.y = GetSpinControl(IDC_RANDOM_POS_Y_SPIN);
	randomPosition_.z = GetSpinControl(IDC_RANDOM_POS_Z_SPIN);

	//スピード
	speed_ = GetSpinControl(IDC_SPEED_SPIN);

	//公転スピード
	revolutionSpeed_ = GetSpinControl(IDC_REVOLUTION_SPEED_SPIN);

	//動きの種類
	if (BST_CHECKED == SendMessage(GetDlgItem(hDlg, IDC_ATTRACT_RADIO), BM_GETCHECK, 0, 0))
	{
		moveTypeState_ = ATTRACT;
	}
	if (BST_CHECKED == SendMessage(GetDlgItem(hDlg, IDC_PULL_APART_RADIO), BM_GETCHECK, 0, 0))
	{
		moveTypeState_ = PULL_APART;
	}

	//公転の固定軸
	axis_.x = GetSpinControl(IDC_REVORUTION_X_SPIN);
	axis_.y = GetSpinControl(IDC_REVORUTION_Y_SPIN);
	axis_.z = GetSpinControl(IDC_REVORUTION_Z_SPIN);
	D3DXVec3Normalize(&axis_, &axis_);
	
	isAxisChange_ = true;

	//距離によってアルファ値を変えるか
		//zバッファを適用するか
	if (BST_CHECKED == SendMessage(GetDlgItem(hDlg, IDC_DISTANCE_ALPHA_CHECK), BM_GETCHECK, 0, 0))
	{
		isDistanceAlpha_ = true;
	}
	else
	{
		isDistanceAlpha_ = false;
	}

	//ループするか
	if (BST_CHECKED == SendMessage(GetDlgItem(hDlg, IDC_LOOP_CHECK), BM_GETCHECK, 0, 0))
	{
		isLoop_ = true;
	}
	else
	{
		isLoop_ = false;
	}

	//ひきつける動きをする際にパーティクルを生成する範囲
	attractGenerateRange_ = GetSpinControl(IDC_LOOP_RANGE_SPIN);

	//重力
	gravity_ = GetSpinControl(IDC_GRAVITY_SPIN);

	//回転を使うか
	if (BST_CHECKED == SendMessage(GetDlgItem(hDlg, IDC_REVORUTION_CHECK), BM_GETCHECK, 0, 0))
	{
		isRevolution_ = true;
	}
	else
	{
		isRevolution_ = false;
	}

	//拡散度
	diffusivity_ = GetSpinControl(IDC_DIFFUSIVITY_SPIN);

	//何フレームでループするか
	loopCount_ = GetSpinControl(IDC_LOOP_COUNT_SPIN);
}

void Particle::SetTexture(LPCSTR fileName)
{
	//新しくパーティクルをセットするために一度deleteする
	SAFE_RELEASE(pTexture_);
	if (fileName != nullptr)
	{
		D3DXCreateTextureFromFile(Direct3D::pDevice, fileName, &pTexture_);
		assert(pTexture_ != nullptr);
	}
}

void Particle::SetTexture(TEXTURE_ENUM textureEnum)
{
	SetTexture(textureFileName[textureEnum]);
}

void Particle::Draw()
{
	Direct3D::pDevice->SetTexture(0, pTexture_);

	//構造体がなんの頂点情報を含んでいるか
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_PSIZE | D3DFVF_DIFFUSE);

	//使用する頂点バッファを登録
	Direct3D::pDevice->SetStreamSource(
		0,								//複数種類頂点バッファを作っているときにどのバッファを使うかを指定 その場合SetFVFによる頂点情報の指定ができないのでD3DVERTEXELEMENT9を使う
		pVertexBuffer_,					//使用するバッファ
		0 * sizeof(ParticleVertexData),	//どこから読み込みを始めるか 複数モデルを読み込んでいるときなどに使う
		sizeof(ParticleVertexData)		//構造体のサイズ
	);

	//描画 描画するプリミティブの種類, 描画を始める位置, 何枚ポリゴンを出すか
	Direct3D::pDevice->DrawPrimitive(D3DPT_POINTLIST, 0, drawCount_);
}

int Particle::GetMaxCapacity()
{
	return MAX_CAPACITY;
}








