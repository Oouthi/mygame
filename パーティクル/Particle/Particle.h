#pragma once
#include <d3dx9.h>

//パーティクルの頂点情報
struct ParticleVertexData
{
	//DirectXに渡す頂点情報
	D3DXVECTOR3 position;	//位置
	float size;				//サイズ
	D3DCOLOR color;			//色
	
	//自分で使う頂点情報
	D3DXVECTOR3 moveDir;	//動く方向
	int loopCount;			//ループするまでのカウント
};

//既存のテクスチャを使うときに使う
enum TEXTURE_ENUM
{
	CIRCLE = 0,			//円
	TEXTURE_ENUM_MAX
};

//動きの種類
enum MOVE_TYPE
{
	ATTRACT = 0,	//ひきつける
	PULL_APART,		//遠ざける
	MOVE_TYPE_MAX
};

class Particle
{
	//VRAMにある頂点情報にアクセスするためのポインタ
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	
	//パーティクルのデータ
	ParticleVertexData* pVertexData_;

	//パーティクルに使うテクスチャ
	IDirect3DTexture9* pTexture_;

	//パーティクルの中心位置
	D3DXVECTOR3 position_;

	//起点となる位置
	D3DXVECTOR3 basePosition_;

	//ランダムに動かす位置
	D3DXVECTOR3 randomPosition_;

	//パーティクルを出す方向や回転の軸
	D3DXVECTOR3 axis_;
	
	//カラーの基準値
	D3DXCOLOR baseColor_;

	//カラーのランダム範囲
	D3DXCOLOR randomColor_;

	//表示するパーティクルの最大数
	int maxDrawCount_;

	//表示するパーティクルの数
	int drawCount_;

	//1フレームに出すパーティクルの数
	int addDrawCount_;

	//サイズの基準値
	int baseSize_;

	//サイズのランダム範囲
	int randomSize_;

	//パーティクルの移動速度
	int speed_;

	//パーティクルの回転速度
	int revolutionSpeed_;

	//ひきつける動きをする際にパーティクルを生成する範囲
	int attractGenerateRange_;

	//重力のつよさ
	int gravity_;

	//拡散度
	int diffusivity_;

	//ループするまでのフレーム数
	int loopCount_;

	//サイズを変えたとき
	bool isSizeChange_;

	//カラーを変えたとき
	bool isColorChange_;

	//パーティクルの生成方向・回転軸を変えたとき
	bool isAxisChange_;

	//ソートをするか
	bool isSort_;

	//ループするか
	bool isLoop_;

	//距離によってアルファ値を変えるか
	bool isDistanceAlpha_;

	//回転させるか
	bool isRevolution_;
	
	//動きの状態
	MOVE_TYPE moveTypeState_;

	//動きの関数を入れる関数ポインタ
	void(Particle::*moveFuncArray_[MOVE_TYPE_MAX])();

public:
	Particle();
	~Particle();

	//表示
	void Draw();

	//頂点情報の変更を行う
	void Update();

	//パーティクルを移動させる
	void UpdateVertexData();

	//アルファブレンドを正しく行うためには
	//ビュー行列の視線ベクトルに投影した場合の距離が最も遠い頂点から描画しなければならないため
	//昇順に頂点を並び替える
	void DistanceQuickSort();

	//頂点バッファを作る
	void CreateVertexBuffer();

	//表示するパーティクルを追加
	void AddParticle();

	//中心位置を移動させる
	void UpdatePosition();

	//パーティクルを中心点から放出する動き
	void Attract();

	//パーティクルをを中心点に引き寄せる動き
	void PullApart();

	//色を変更する
	//引数 変更したい配列の最初の要素, 変更したい配列の最後の要素
	void ChangeColor(int start, int end);

	//サイズを変更する
	//引数 変更したい配列の最初の要素, 変更したい配列の最後の要素
	void ChangeSize(int start, int end);

	//ダイアログの情報から各項目をセットする
	//ダイアログのウィンドウハンドル, 変更された項目のID
	void CommandProcess(HWND hDlg, WPARAM wp);

	//テクスチャをセットする
	//テクスチャのファイルパス
	void SetTexture(LPCSTR filePath);
	//既存のテクスチャをセットする
	//引数 使いたい既存テクスチャの番号
	void SetTexture(TEXTURE_ENUM textureEnum);

	//このシステムで出せるパーティクルの最大数を返す
	//戻り値 このシステムで出せるパーティクルの最大数
	int GetMaxCapacity();
};

