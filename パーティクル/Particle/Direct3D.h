#pragma once
#include <d3dx9.h>

namespace Direct3D
{
	//Direct3Dを使うためのデバイス
	extern LPDIRECT3DDEVICE9 pDevice;

	//初期化
	void Initialize(HWND hWnd);

	//表示開始
	void BeginDraw();

	//表示終了
	void EndDraw();

	//解放処理
	void Release();

	//ビュー行列の設定
	//引数 : 位置、 どこを見るか, 視点の回転
	void ViewTransform(D3DXVECTOR3 position, D3DXVECTOR3 target, D3DXVECTOR3 rotate);

	//カメラの位置取得
	D3DXVECTOR3 GetCameraPosition();

	//カメラのターゲット取得
	D3DXVECTOR3 GetCameraTarget();

	//ダイアログで設定の変更があったときに呼ばれる
	void CommandProcess(HWND hDlg, WPARAM wp);
};

