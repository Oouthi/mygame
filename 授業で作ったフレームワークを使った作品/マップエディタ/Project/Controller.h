#pragma once
#include <stack>
#include "Engine/IGameObject.h"
#include "Map.h"

//レイキャスト情報構造体
struct RayCastData;

//コントローラーを管理するクラス
class Controller : public IGameObject
{
	//今選んでいるブロック
	//ブロックを設置するときに使う
	BLOCK_ENUM selectBlock_;

	//画面中心の目印画像の番号
	int hSenterPointPict_;

	//SelectBlock文字画像の番号
	int hSelectBlockPict_;

	//ブロックの種類文字画像の番号
	int hBlockPict_[BLOCK_MAX];

	//アンドゥ用スタック ブロックを追加するとtrue、消すとfalseが入る
	std::stack<bool> undoStack;
	
	//リドゥ用スタック アンドゥでブロックを消すとtrue、消すとfalseが入る
	std::stack<bool> redoStack;

	//消したブロックの情報を入れるスタック アンドゥで使う
	std::stack<BlockInfo> undoBlockStack;

	//アンドゥで消したブロックの情報を入れるスタック リドゥで使う
	std::stack<BlockInfo>redoBlockStack;

public:
	//コンストラクタ
	Controller(IGameObject* parent);

	//デストラクタ
	~Controller();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//今選んでいるブロック変数のゲッター
	//戻り値:BLOCK_ENUM型
	BLOCK_ENUM GetSelectBlock()
	{
		return selectBlock_;
	}

	//今選んでいるブロック変数のセッター
	//引数:BLOCK_ENUM型
	void SetSelectBlock(BLOCK_ENUM selectBlock)
	{
		selectBlock_ = selectBlock;
	}

	//アンドゥに使うスタックの削除
	void InitUndoStack();

	//リドゥに使うスタックのの削除
	void InitRedoStack();

private:

	//ブロックを置く位置を決めてmapクラスにブロックを追加させる
	void DecideBlockToAdd();

	//削除するブロックを決めてmapクラスに削除させる
	//ブロックを置く関数と処理が似てるため共通部分を抜き出そうとしたが
	//使う変数やfor分の中で呼び出す処理が違ったため難しかった
	void DecideBlockToRemove();

	//全てのブロックとレイキャストをする
	//第一引数:当たったブロックの面法線や当たったかどうかの情報を取得するためのRayCastData型のポインタ
	//第二引数:当たったブロックの配列番号を取得するためのint型のポインタ
	void BlockRayCast(RayCastData& rayCastData, int& minDistanseBlockNumber, bool& isHit);
	

	//アンドゥ
	void Undo();

	//リドゥ
	void Redo();

	//移動
	void Move();

	//マウスカーソルを表示 / 非表示にする
	void SwitchDrawCursor();
};