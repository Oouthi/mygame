#include <vector>
#include <string>
#include "FileOperation.h"
#include "Engine/SceneManager.h"
#include "PlayScene.h"
#include "Map.h"
#include "Controller.h"

namespace FileOperation
{
	//今開いているファイルのパス
	std::string nowOpenFilePath;

	void NewSave()
	{
		//ファイルパスを入れる変数 ファイル保存ダイアログを出したときデフォルトで入れる名前で初期化
		char filePath[MAX_PATH] = "無題.map";

		//名前をつけて保存ダイアログの設定用構造体
		OPENFILENAME ofn;

		//「ファイルを保存」ダイアログの設定
		ZeroMemory(&ofn, sizeof(ofn));			//構造体初期化
		ofn.lStructSize = sizeof(OPENFILENAME);	//構造体のサイズ
		ofn.lpstrFilter = TEXT("マップデータ(*.map)\0*.map\0")		//─┬ファイルの種類
						  TEXT("すべてのファイル(*.*)\0*.*\0\0");	//─┘
		ofn.lpstrFile = (LPSTR)filePath;		//ファイル名
		ofn.nMaxFile = MAX_PATH;				//パスの最大文字数
		ofn.Flags = OFN_OVERWRITEPROMPT;		//フラグ（同名ファイルが存在したら上書き確認）
		ofn.lpstrDefExt = "map";				//デフォルト拡張子

		//保存 / キャンセルフラグ
		BOOL selFile;

		//「ファイルを保存」ダイアログ生成
		selFile = GetSaveFileName(&ofn);

		//キャンセルしたら中断
		if (selFile == FALSE)
		{
			return;
		}

		//保存するを選んだらファイルにデータを書き込む
		Save(filePath);

		//今開いているファイルのパスを保存する
		nowOpenFilePath = filePath;
	}

	void OverwritingSave()
	{
		//まだファイルを保存していなければ新しく保存をする
		if (nowOpenFilePath.length() == 0)
		{
			NewSave();
		}
		else
		{
			Save(nowOpenFilePath);
		}
	}

	void Save(std::string filePath)
	{
		//ブロックの情報が入った配列をプレイシーンを通して取ってくる
		const std::vector<BlockInfo> blockVector = ((PlayScene*)(SceneManager::GetCurrentScene()))->GetMap()->GetBlockVector();

		//ファイルのハンドル
		HANDLE hFile;	

		//ファイルのハンドルにファイルの開き方を設定する
		hFile = CreateFile(
			filePath.c_str(),		//ファイルのパス
			GENERIC_WRITE,			//アクセスモード（書き込み用）
			0,						//共有（なし）
			NULL,					//セキュリティ属性（継承しない）
			CREATE_ALWAYS,			//作成方法
			FILE_ATTRIBUTE_NORMAL,	//属性とフラグ（設定なし）
			NULL);

		//書き込み位置
		DWORD dwBytes = 0;

		for (int i = 0; i < blockVector.size(); i++)
		{
			//書き込む文字を入れる変数にブロックの種類と位置の情報を入れる
			//データが大きくならないように整数値で記録する
			std::string data = 
				std::to_string((blockVector)[i].type) + "," +
				std::to_string((int)round((blockVector)[i].position.x)) + "," +
				std::to_string((int)round((blockVector)[i].position.y)) + "," +
				std::to_string((int)round((blockVector)[i].position.z)) + ",";

			//ファイルに書き込む
			WriteFile(
				hFile,					//ファイルハンドル
				data.c_str(),			//書き込む文字列
				(DWORD)data.length(),	//書き込む文字数
				&dwBytes,				//書き込んだ位置を入れる変数
				NULL);					//オーバーラップド構造体（今回は使わない）
		}

		//書き込んだら閉じる
		CloseHandle(hFile);
	}

	void Load()
	{
		//ファイル名
		char filePath[MAX_PATH] = "";  

		//開くダイアログの設定用構造体
		OPENFILENAME ofn;
		//「ファイルを読み込み」ダイアログの設定
		ZeroMemory(&ofn, sizeof(ofn));//構造体初期化
		ofn.lStructSize = sizeof(OPENFILENAME);	//構造体のサイズ
		ofn.lpstrFilter = TEXT("マップデータ(*.map)\0*.map\0")		//─┬ファイルの種類
						  TEXT("すべてのファイル(*.*)\0*.*\0\0");	//─┘
		ofn.lpstrFile = filePath;				//ファイル名
		ofn.nMaxFile = MAX_PATH;				//パスの最大文字数
		ofn.Flags = OFN_FILEMUSTEXIST;			//フラグ 既存のファイル名しか指定できない
		ofn.lpstrDefExt = "map";				//デフォルト拡張子

		//ファイルを開く / キャンセルフラグ
		BOOL selFile;

		//「ファイルを開く」ダイアログ生成
		selFile = GetOpenFileName(&ofn);

		//キャンセルしたら中断
		if (selFile == FALSE)
		{
			return;
		}

		//ファイルのハンドル
		HANDLE hFile;        

		//ファイルのハンドルにファイルの開き方を設定する
		hFile = CreateFile(
			filePath,				//ファイル名
			GENERIC_READ,			//アクセスモード（読み込み用）
			0,						//共有（なし）
			NULL,					//セキュリティ属性（継承しない）
			OPEN_EXISTING,			//作成方法
			FILE_ATTRIBUTE_NORMAL,	//属性とフラグ（設定なし）
			NULL);					//拡張属性（なし）

		if (hFile == INVALID_HANDLE_VALUE)
		{
			return;
		}

		//ファイルのサイズ
		DWORD fileSize;
		fileSize = GetFileSize(hFile, NULL);
		//ファイルのデータ
		char* data = nullptr;
		//ファイルのサイズ分メモリを確保
		data = new char[fileSize];

		//ファイルからデータをロードする
		//読み込み位置
		DWORD dwBytes = 0; 
		ReadFile(
			hFile,     //ファイルハンドル
			data,      //データを入れる変数
			fileSize,  //読み込むサイズ
			&dwBytes,  //読み込んだサイズ
			NULL);     //オーバーラップド構造体（今回は使わない）

		//コントローラーのリドゥ、アンドゥに使うスタックを削除
		Controller* pController = ((PlayScene*)SceneManager::GetCurrentScene())->GetController();
		pController->InitUndoStack();
		pController->InitRedoStack();

		//ブロック情報が入った配列操作用
		Map* pMap = ((PlayScene*)SceneManager::GetCurrentScene())->GetMap();
		pMap->InitializeBlockVector();

		//ブロックの情報を入れる配列に要素を足していく
		//indexは読み込み位置
		for(int index = 0; index < fileSize;)
		{
			//要素を足すときに使う構造体
			BlockInfo blockInfo;
			//データの要素を入れる
			std::string dataIndex;

			//データはブロックの種類、コンマ(,)、位置x、コンマ(,)、位置y、コンマ(,)、位置z、コンマ(,)の順番にループして入っている
			//なのでatoiでコンマまで読み込んだあとstringに入れてlengthで文字数を取得し、文字数 + コンマ分(1文字)読み込み位置を進める
			dataIndex = std::to_string(blockInfo.type = atoi(data + index));
			//読み込み位置を進める
			index += dataIndex.length() + 1;

			dataIndex = std::to_string((int)(blockInfo.position.x = atoi(data + index)));
			//読み込み位置を進める
			index += dataIndex.length() + 1;

			dataIndex = std::to_string((int)(blockInfo.position.y = atoi(data + index)));
			//読み込み位置を進める
			index += dataIndex.length() + 1;

			dataIndex = std::to_string((int)(blockInfo.position.z = atoi(data + index)));
			//読み込み位置を進める
			index += dataIndex.length() + 1;

			pMap->AddBlock(blockInfo);
		}

		//読み込みが終わったら閉じる
		CloseHandle(hFile);

		//	//今開いているファイルのパスを保存する
		nowOpenFilePath = filePath;

		//データを保存していた変数の開放
		SAFE_DELETE_ARRAY(data);
	}
}