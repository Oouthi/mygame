#pragma once
#include <vector>
#include "Engine/IGameObject.h"

const int BLOCK_SIZE = 1;

//ブロックの種類のenum
enum BLOCK_ENUM
{
	BLOCK_BRICK = 0,
	BLOCK_DEFAULE,
	BLOCK_GRASS,
	BLOCK_SAND,
	BLOCK_WATER,
	BLOCK_MAX,
};

//ブロックの情報
struct BlockInfo
{
	//ブロックの種類
	int type;
	//位置
	D3DXVECTOR3 position;
};

//マップを管理するクラス
class Map : public IGameObject
{
private:

	//ステージ
	std::vector<BlockInfo> blockVector_;

	//ブロックのモデル番号
	int hBlockModel_[BLOCK_MAX];

public:
	Map(IGameObject* parent);

	//デストラクタ
	~Map();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ブロックの情報が入っている配列のゲッター
	//戻り値:std::vector<BLOCK_INFO>*型のブロック情報が入っている配列のポインタ constなので中身の変更はできない
	std::vector<BlockInfo> GetBlockVector()
	{
		return blockVector_;
	}

	//ブロックの情報が入っている配列に要素を追加する
	//引数:int型のブロックの種類を入れる変数, D3DXVECTOR3型の位置を入れる変数
	void AddBlock(BlockInfo blockInfo);

	//ブロックの情報が入っている配列の要素を削除する
	//引数:削除したいブロックが入っている配列の番号
	void RemoveBlock(int number);

	//ブロックの情報が入っている配列を空にする
	void InitializeBlockVector();
};