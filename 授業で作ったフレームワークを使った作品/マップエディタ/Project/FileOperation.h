#pragma once
#include "Engine/IGameObject.h"

namespace FileOperation
{
	//ファイルはブロックの種類、コンマ(,)、ブロックのポジションx・y・zの順番で記録される

	//新しくセーブ
	void NewSave();

	//上書きセーブ
	void OverwritingSave();

	//新しくセーブと上書きセーブの共通部分
	//引数:セーブするファイルのパス
	void Save(std::string filePath);

	void Load();
}

