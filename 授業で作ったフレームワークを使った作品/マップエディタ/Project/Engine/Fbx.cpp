#include "Fbx.h"
#include "Direct3D.h"



Fbx::Fbx(LPD3DXEFFECT pEffect) :pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pTexture_(nullptr),
pMaterial_(nullptr), pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr),
vertexCount_(0), polygonCount_(0), indexCount_(0), materialCount_(0), polygonCountOfMaterial_(nullptr),
pEffect_(pEffect)
{																								

}


Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_DELETE_ARRAY(pMaterial_);
	//SAFE_RELEASE(pTexture_);
	//newした分だけでリリースしないといけない
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(pTexture_[i]);
	}
	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);
	SAFE_RELEASE(pVertexBuffer_);
	pScene_->Destroy();
	pManager_->Destroy();
}

void Fbx::Load(const char* imagePass)
{
	//ファイルを読み込むのに必要なやつら
	pManager_ = FbxManager::Create();					//ファイルを読み込むやつらをまとめるやつ
	pImporter_ = FbxImporter::Create(pManager_, "");	//ファイルを読み込むだけのやつ(管理はできない)
	pScene_ = FbxScene::Create(pManager_, "");			//読み込んだファイルを管理するやつ

	//ファイルを読み込む
	pImporter_->Initialize(imagePass);
	//読み込んだファイルをpSceneにセットする
	pImporter_->Import(pScene_);
	//ファイルの読み込みが終わったのでインポーターの役目は終了
	pImporter_->Destroy();

	//現在のカレントディレクトリの取得
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリをファイルがある位置に変更
	char dir[MAX_PATH];
	_splitpath_s(imagePass, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);

	//ルートノードを取得
	FbxNode* rootNode = pScene_->GetRootNode();
	//ルートノードに子がいるか調べる
	int childCount = rootNode->GetChildCount();
	for (int i = 0; childCount > i; i++)
    {
        //ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
    }

	SetCurrentDirectory(defaultCurrentDir);	//カレントディレクトリに戻す
}

void Fbx::Draw(const D3DXMATRIX &matrix)
{
	//頂点バッファの設定
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);
	//頂点ストリームを指定
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	//ワールド行列をセット
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	//fbxが読み込めなければここで止まる
	assert(pVertexBuffer_);

	//マテリアルごとに描画
	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);

		//mayaで設定した色を渡す
		if (pEffect_ != nullptr)
		{
			if (pTexture_ == nullptr)
			{
				pEffect_->SetBool("IS_TEXTURE", false);
			}
			else
			{
				pEffect_->SetBool("IS_TEXTURE", true);
			}

			pEffect_->SetVector("DIFFUSE_COLOR", &D3DXVECTOR4(pMaterial_[i].Diffuse.r,
				pMaterial_[i].Diffuse.g,
				pMaterial_[i].Diffuse.b,
				pMaterial_[i].Diffuse.a));

			pEffect_->SetVector("AMBIENT_COLOR", &D3DXVECTOR4(pMaterial_[i].Ambient.r,
				pMaterial_[i].Ambient.g,
				pMaterial_[i].Ambient.b,
				pMaterial_[i].Ambient.a));

			pEffect_->SetVector("SPECULER_COLOR", &D3DXVECTOR4(pMaterial_[i].Specular.r,
				pMaterial_[i].Specular.g,
				pMaterial_[i].Specular.b,
				pMaterial_[i].Specular.a));

			pEffect_->SetFloat("SPECULER_POWER", pMaterial_[i].Power);
		}
		else
		{
			Direct3D::pDevice->SetMaterial(&pMaterial_[i]);
		}

		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);

	}
}

void Fbx::CheckNode(FbxNode * pNode)
{
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	//メッシュノードかどうか
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//ノードの情報を入れる
		materialCount_ = pNode->GetMaterialCount();
		pMaterial_ = new D3DMATERIAL9[materialCount_];
		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];

		/////////////////////////////////////////////
		//for (int i = 0; i < materialCount_; i++)	/			
		//{											/
		//	pTexture_[i] = nullptr;					/
		//}											/
		/////////////////////////////////////////////

		ZeroMemory(pTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);

		for (int i = 0; i < materialCount_; i++)
		{
			//MAYAで設定したマテリアルを取ってくる
			//Phong型を使っているのはLambertを代入することも出来るため
			FbxSurfacePhong* surface = (FbxSurfacePhong*)pNode->GetMaterial(i);
			FbxDouble3 diffuse = surface->Diffuse;	//拡散反射光
			FbxDouble3 ambient = surface->Ambient;	//環境光

			ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

			//ポリゴンの色
			pMaterial_[i].Diffuse.r = (float)diffuse[0];
			pMaterial_[i].Diffuse.g = (float)diffuse[1];
			pMaterial_[i].Diffuse.b = (float)diffuse[2];
			pMaterial_[i].Diffuse.a = 1.0f;	//不透明度

			//環境光
			pMaterial_[i].Ambient.r = (float)ambient[0];
			pMaterial_[i].Ambient.g = (float)ambient[1];
			pMaterial_[i].Ambient.b = (float)ambient[2];
			pMaterial_[i].Ambient.a = 1.0f;

			//鏡面反射光
			//MAYAでランバードシェーダーでマテリアル設定していると
			//Specularの設定がないため_pMaterialに入れた後呼び出すとエラーになる
			//FbxSurface型には番号がついているためPhong型かそれ以外か調べる
			if (surface->GetClassId().Is(FbxSurfacePhong::ClassId))
			{
				FbxDouble3 specular = surface->Specular;
				pMaterial_[i].Specular.r = (float)specular[0];
				pMaterial_[i].Specular.g = (float)specular[1];
				pMaterial_[i].Specular.b = (float)specular[2];
				pMaterial_[i].Specular.a = 1.0f;

				pMaterial_[i].Power = (float)surface->Shininess;
			}

			//テクスチャの情報
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			//テクスチャを使ていない場合
			if (textureFile == nullptr)
			{
				pTexture_[i] = nullptr;
			}
			//テクスチャを使っている場合
			else
			{
				const char* textureFileName = textureFile->GetFileName();

				char name[_MAX_FNAME];      //ファイル名
				char ext[_MAX_EXT];         //拡張子
				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);
				
				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
				D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);

				//ポインタの中身がcdcdcd...の場合は入っていない
				assert(pTexture_ != nullptr);
			}
		}

		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh * pMesh)
{
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	//各情報を調べる
	vertexCount_ = pMesh->GetControlPointsCount();
	polygonCount_ = pMesh->GetPolygonCount();
	indexCount_ = pMesh->GetPolygonVertexCount();
	//頂点情報
	Vertex* vertexList = new Vertex[vertexCount_];

	//一つずつ頂点の位置を入れていく
	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];			
	}

	//ポリゴンを一枚ずつ調べる
	for (int i = 0; i < polygonCount_; i++)
	{
		//データの先頭を取得する
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		//3頂点分
		for (int j = 0; j < 3; j++)
		{
			//頂点のUV
			//インデックスバッファからインデックスを取得する
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			//頂点の法線
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			//UV値セット
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	//空のバッファを作る
	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);

	assert(pVertexBuffer_ != nullptr);

	//ロックする
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	//空のバッファにデータコピー
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	//終わったらアンロック
	pVertexBuffer_->Unlock();

	SAFE_DELETE_ARRAY(vertexList);

	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	polygonCountOfMaterial_ = new int[materialCount_];
	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];
		int count = 0;
		//インデックス情報を取得し、マテリアルの数だけインデックスバッファを作る
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//今見ているポリゴンが何個目のマテリアルか確認
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				//今見ている三角形のポリゴンをインデックスリストに追加
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		//countを３で割ればポリゴンの数になる
		polygonCountOfMaterial_[i] = count / 3;

		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_ != nullptr);
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();
		SAFE_DELETE_ARRAY(indexList);
	}
}

void Fbx::RayCast(RayCastData &data, const D3DXMATRIX &matrix)
{
	data.hit = false;

	//頂点バッファをロック
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//レイの発射位置から一番近い面までの距離を入れる
	float minDistance;

	//マテリアル毎
	for (DWORD i = 0; i < materialCount_; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < polygonCountOfMaterial_[i]; j++)
		{
			//3頂点
			D3DXVECTOR3 v0, v1, v2;

			//頂点の位置にワールド行列をかける
			v0 = vCopy[iCopy[j * 3 + 0]].pos;
			D3DXVec3TransformCoord(&v0, &v0, &matrix);

			v1 = vCopy[iCopy[j * 3 + 1]].pos;
			D3DXVec3TransformCoord(&v1, &v1, &matrix);

			v2 = vCopy[iCopy[j * 3 + 2]].pos;
			D3DXVec3TransformCoord(&v2, &v2, &matrix);

			// ※ここで、D3DXIntersectTri関数を使う
			BOOL hit = D3DXIntersectTri(&v0, &v1, &v2, &data.origin, &data.direction, nullptr, nullptr, &data.hitDistance);
			if (hit)
			{
				//最初に当たった時
				if (data.hit == false)
				{
					data.hit = true;
					minDistance = data.hitDistance;

					//当たった面の法線を引数で受け取ったレイキャストデータに入れる
					D3DXVec3Cross(&data.normal, &(v0 - v1), &(v0 - v2));
				}

				//いまminDistanceに入っている距離よりさらにレイキャストの発射位置に近い面があれば更新する
				if (data.hitDistance < minDistance)
				{
					minDistance = data.hitDistance;

					//当たった面の法線を引数で受け取ったレイキャストデータに入れる
					D3DXVec3Cross(&data.normal, &(v0 - v1), &(v0 - v2));
				}
			}

		}

		//インデックスバッファ使用終了
		ppIndexBuffer_[i]->Unlock();
	}

	//当たっていたら最後にレイキャストデータのhitDistanceにminDistanceを入れる
	if (data.hit)
	{
		data.hitDistance = minDistance;
	}
}