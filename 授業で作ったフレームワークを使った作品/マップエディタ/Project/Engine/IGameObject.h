#pragma once
#include <d3dx9.h>
#include <list>
#include <string.h>

class Collider;

class IGameObject
{
protected:
	IGameObject* pParent_;	//親の情報
	std::list < IGameObject* > childList_;
	std::string name_;

	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	D3DXMATRIX localMatrix_;	//自分視点の行列親のworldMatrix_をかけ入れる
	D3DXMATRIX worldMatrix_;	//世界からみた行列

	bool dead_;
	bool isUpdate_;

	//衝突判定リスト
	std::list<Collider*>	colliderList_;	

public:
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);

	//仮想デストラクタにしないと子クラスのデストラクタが呼ばれず親クラスのデストラクタが呼ばれる
	virtual ~IGameObject();

	//初期化
	virtual void Initialize() = 0;
	//更新
	virtual void Update() = 0;
	//描写
	virtual void Draw() = 0;
	//解放
	virtual void Release() = 0;

	//更新
	void UpdateSub();
	//描写
	void DrawSub();
	//解放
	void ReleaseSub();

	void KillMe();

	

	//テンプレートクラスを使うことで汎用的にできる
	template<class T>
	//<>の中に書いたクラスに置き換える
	T* CreateGameObject(IGameObject* parent)	//ゲームオブジェクトをvectorに入れる
	{
		T* p = new T(parent);
		parent->PushBackChild(p);
		p->Initialize();

		return p;
	}

	void Transform();

	void IGameObject::PushBackChild(IGameObject* pObj);

	void SetIsUpdate(bool isUpdate)
	{
		isUpdate_ = isUpdate;
	}

	void SetPosition(D3DXVECTOR3 position)
	{
		position_ = position;
	}

	D3DXVECTOR3 GetPosition()
	{
		return position_;
	}

	void SetRotate(D3DXVECTOR3 rotate)
	{
		rotate_ = rotate;
	}

	D3DXVECTOR3 GetRotate()
	{
		return rotate_;
	}

	D3DXVECTOR3 GetScale()
	{
		return scale_;
	}

	std::string GetName()
	{
		return name_;
	}

	//コライダー（衝突判定）を追加する
	void AddCollider(Collider * collider);

	//何かと衝突した場合に呼ばれる（オーバーライド用）
	//引数：pTarget	衝突した相手
	virtual void OnCollision(IGameObject* pTarget) {};

	//衝突判定
	//引数：pTarget	衝突してるか調べる相手
	void Collision(IGameObject* pTarget);

	//オブジェクトの名前をとってくる
	//戻り値:std::string型の名前
	std::string GetObjectName();

	//オブジェクトを名前で検索する
	//引数:const std::string型の探したいオブジェクトの名前
	//戻り値:IGameObject*型のオブジェクトのポインタ 見つからなければnullを返す
	IGameObject* FindChildObject(const std::string & name);

	//テスト用の衝突判定枠を表示
	void CollisionDraw();

	Collider* GetColliderList(int i)
	{
		auto it = colliderList_.begin();
		for(int j = 0; j < i; j++)
		{
			it++;
		}
		return (*it);
	}
};				 


