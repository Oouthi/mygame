#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "IGameObject.h"
#include "SceneManager.h"
#include "Input.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete [] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}


struct WindowSize
{
	int width;
	int height;
};

//ゲーム全体で使われるファイルをまたいで保持したい値を管理する
namespace Global
{
	//ウィンドウのサイズを設定する
	//引数:WindowSize型のウィンドウのサイズ
	void SetWindowSize(WindowSize windowSize);

	//ウィンドウサイズを取得する
	//戻り値:WindowSize型のウィンドウのサイズ
	WindowSize GetWindowSize();
};
