#include "BoxCollider.h"
#include "Direct3D.h"

//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
BoxCollider::BoxCollider(const std::string& name, D3DXVECTOR3 basePos, D3DXVECTOR3 size , IGameObject* pGameObject) :
							Collider(name, pGameObject)
{
	rocalCentor_ = basePos;
	WorldRotateAndPosCalc();
	size_ = size;
	type_ = COLLIDER_BOX;

	m_NormaDirect_[DIR_X] = D3DXVECTOR3(1, 0, 0);
	m_NormaDirect_[DIR_Y] = D3DXVECTOR3(0, 1, 0);
	m_NormaDirect_[DIR_Z] = D3DXVECTOR3(0, 0, 1);

	//引数で入ってくるのは直径だが計算するときは半径なので半分にしていれる
	m_fLength[DIR_X] = size.x / 2;
	m_fLength[DIR_Y] = size.y / 2;
	m_fLength[DIR_Z] = size.z / 2;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, size.x, size.y, size.z, &pMesh_, 0);
#endif
}

BoxCollider::BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size, IGameObject* pGameObject) : BoxCollider("", basePos, size, pGameObject)
{
}

void BoxCollider::WorldRotateAndPosCalc()
{
	//オブジェクトが回転したらコライダーの位置も回転しなければいけない
	D3DXMATRIX mX, mY, mZ;
	D3DXMatrixRotationX(&mX, D3DXToRadian(pGameObject_->GetRotate().x));   //X軸で45°回転させる行列
	D3DXMatrixRotationY(&mY, D3DXToRadian(pGameObject_->GetRotate().y));   //Y軸で90°回転させる行列
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(pGameObject_->GetRotate().z));   //Z軸で30°回転させる行列

	D3DXMATRIX mat;
	D3DXMatrixIdentity(&mat);
	mat = mX * mY * mZ;

	D3DXVec3TransformCoord(&worldCenter_, &rocalCentor_, &mat);

	worldCenter_ += pGameObject_->GetPosition();

	worldRotate_ = loaclRotate_ + pGameObject_->GetRotate();

	//面の法線ベクトルも変える
	D3DXMatrixIdentity(&mat);

	D3DXMatrixRotationX(&mX, D3DXToRadian(worldRotate_.x));
	D3DXMatrixRotationY(&mY, D3DXToRadian(worldRotate_.y));
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(worldRotate_.z));
	
	mat = mX * mY * mZ;

	m_NormaDirect_[DIR_X] = D3DXVECTOR3(1, 0, 0);
	m_NormaDirect_[DIR_Y] = D3DXVECTOR3(0, 1, 0);
	m_NormaDirect_[DIR_Z] = D3DXVECTOR3(0, 0, 1);

	D3DXVec3TransformCoord(&m_NormaDirect_[DIR_X], &m_NormaDirect_[DIR_X], &mat);
	D3DXVec3TransformCoord(&m_NormaDirect_[DIR_Y], &m_NormaDirect_[DIR_Y], &mat);
	D3DXVec3TransformCoord(&m_NormaDirect_[DIR_Z], &m_NormaDirect_[DIR_Z], &mat);

	for (int dir = DIR_X; dir < DIR_QUANTITY; dir++)
	{
		D3DXVec3Normalize(&m_NormaDirect_[dir], &m_NormaDirect_[dir]);
	}
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool BoxCollider::IsHit(Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsBox((BoxCollider*)target, this);
	else
		return IsHitBoxVsCircle(this, (SphereCollider*)target);
}