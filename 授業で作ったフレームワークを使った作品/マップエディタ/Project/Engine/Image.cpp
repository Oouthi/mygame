#include <vector>
#include "Image.h"

namespace Image
{
	//モデルデータリストの配列
	std::vector<ImageData*> dataList;

	int Load(std::string fileName)
	{
		//モデルデータ
		ImageData* pData = new ImageData;

		//受け取ったファイルネームをモデルデータに突っ込む
		pData->fileName = fileName;

		//isExist...存在するかどうか
		bool isExist = false;
		//同じ作成を何回もやらないようにするためのループ
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//既に同じファイル名があった場合
			if (dataList[i]->fileName == fileName)
			{
				//pSpriteのアドレス「だけ」をいれる
				pData->pSprite = dataList[i]->pSprite;

				//同じのがあった！
				isExist = true;

				//その後の処理は不要のためブレイク
				break;
			}
		}

		//存在しなかったときの処理
		if (isExist == false)
		{
			//Sprite型のモデルデータを作成
			pData->pSprite = new Sprite;

			//c_str()を使うと、キャラ型のデータに変えてくれる
			pData->pSprite->Load(fileName.c_str());
		}

		//データを入れる
		dataList.push_back(pData);

		//ファイルの番号。-1をすることで正確な値を返せる
		return dataList.size() - 1;
	}

	void Draw(int handle, D3DXCOLOR color)
	{
		dataList[handle]->pSprite->Draw(dataList[handle]->matrix, color);
	}

	void SetMatrix(int handle, D3DXMATRIX &matrix)
	{
		//行列をそのモデルにセット
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{
		{
			//同じSpriteが他にも使ってないか
			bool isExist = false;
			for (unsigned int i = 0; i < dataList.size(); i++)
			{
				//そのSpriteが同じで、i番目のデータリストがNULLじゃなくて、自分同士じゃなければ
				if (i != handle && dataList[i] != nullptr &&
					dataList[i]->pSprite == dataList[handle]->pSprite)
				{
					//処理をスキップ
					isExist = true;
					break;
				}
			}

			//なければ解放
			if (isExist == false)
			{
				SAFE_DELETE(dataList[handle]->pSprite);
			}

			//これは問答無用で解放
			SAFE_DELETE(dataList[handle]);
		}
	}

	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//まだ削除していなければ
			if (dataList[i] != nullptr)
			{
				//解放
				Release(i);
			}
		}
		//全要素消去
		dataList.clear();
	}

	D3DXIMAGE_INFO GetImageInfo(int hPict)
	{
		return dataList[hPict]->pSprite->GetImageInfo();
	}
}