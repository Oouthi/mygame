#include "Model.h"

namespace Model
{
	std::vector<ModelData*> dataList;


	int Load(std::string fileName, LPD3DXEFFECT pEffect)
	{
		ModelData* pData = new ModelData;
		pData->fileName = fileName;

		//すでに同じファイルがdataListにあるか確認
		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//同じファイルだった場合もう一度ファイルを読み込むのは
			//無駄なのでdataListにあるpFbxをpDataのpFbxに代入する
			if (dataList[i]->fileName == fileName)
			{
				pData->pFbx = dataList[i]->pFbx;
				isExist = true;
				break;
			}
		}

		//まだdataListにないファイルだった場合は
		//新しくファイルを読み込む
		if (isExist == false)
		{
			pData->pFbx = new Fbx(pEffect);
			pData->pFbx->Load(fileName.c_str());
			
		}

		//c_str()でchar*で返す
		dataList.push_back(pData);

		return dataList.size() -1;
	}

	void Draw(int handle)
	{
		dataList[handle]->pFbx->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX& matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{
		bool isExist = false;

		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (i != handle && dataList[i] != nullptr && dataList[handle]->pFbx == dataList[i]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pFbx);
		}
		
		SAFE_DELETE(dataList[handle]);
	}

	void AllRelease()
	{
		//ゲーム終了時にすべてのモデルを解放する
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//すでに開放しているところは飛ばす
			if (dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}

	void SetEffect(int handle, LPD3DXEFFECT pEffect)
	{
		dataList[handle]->pFbx->SetEffect(pEffect);
	}

	void RayCast(int handle, RayCastData & data)
	{
		dataList[handle]->pFbx->RayCast(data, dataList[handle]->matrix);
	}

	std::vector<ModelData*> GetModelDataList()
	{
		return dataList;
	}
};
