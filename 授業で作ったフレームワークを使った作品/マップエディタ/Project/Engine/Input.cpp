#include <assert.h>
#include <d3dx9.h>
#include "Input.h"

namespace Input
{
	LPDIRECTINPUT8   pDInput = nullptr;	//Inputでしか使わないのでcpp内で宣言
	
	//キーボード
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	BYTE keyState[256] = { 0 };
	BYTE prevKeyState[256] = { 0 };    //前フレームでの各キーの状態

	//マウス
	LPDIRECTINPUTDEVICE8 pMouseDevice; //デバイスオブジェクト
	DIMOUSESTATE mouseState;    //マウスの状態
	DIMOUSESTATE prevMouseState;   //前フレームのマウスの状態
	HWND hWnd_;		//ウィンドウハンドル 引数で受け取ってきたものと区別するためアンダースコアをつけている

	void Initialize(HWND hWnd)
	{
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		assert(pDInput != nullptr);

		//キーボード
		pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
		assert(pKeyDevice != nullptr);
		//何の操作デバイスか指定
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		
		//マウス
		pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr);
		assert(pKeyDevice);
		pMouseDevice->SetDataFormat(&c_dfDIMouse);
		pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		hWnd_ = hWnd;
	}

	void Update()
	{
		//キーボード
		memcpy(prevKeyState,keyState, sizeof(keyState));
		pKeyDevice->Acquire();	//キーボードを見失う？時にもう一回探す
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		////マウス
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));
		pMouseDevice->Acquire();
		pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState);
	}

	void Release()
	{
		SAFE_RELEASE(pMouseDevice);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}

	///////////////////////////キーボード////////////////////////
	bool IsKey(int keyCode)
	{
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if (keyState[keyCode] & 0x80 && !(prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	bool IsKeyUp(int keyCode)
	{
		//今は放してて、前回は押してる
		if (!(keyState[keyCode] & 0x80) && prevKeyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//////////////////////////////マウス////////////////////////////

		//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスカーソルの位置を取得
	D3DXVECTOR3 GetMousePosition()
	{
		POINT mousePos;	//xyの情報が入っている
		GetCursorPos(&mousePos); //	マウスカーソルの位置をwindowsから取得
		ScreenToClient(hWnd_, &mousePos);	//ディスプレイ全体から見た位置をウィンドウの中で見た位置に変換

		D3DXVECTOR3 result = D3DXVECTOR3(mousePos.x, mousePos.y, 0);
		return result;
	}

	//そのフレームでのマウスの移動量を取得
	D3DXVECTOR3 GetMouseMove()
	{
		D3DXVECTOR3 result(mouseState.lX, mouseState.lY, mouseState.lZ);
		return result;
	}
}