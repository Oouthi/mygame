#pragma once
#include <string>
#include "Sprite.h"

namespace Image
{
	//構造体モデルデータ
	struct ImageData
	{
		std::string fileName;	//STLの文字列を管理する変数
		Sprite*	pSprite;			//FBXファイルを入れる変数
		D3DXMATRIX matrix;		//行列変数

		//コンストラクタ
		ImageData() : fileName(""), pSprite(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	//ファイル名を受け取る関数
	//引数：fileName	ファイルの名前
	//戻値：int			ファイル番号を返す
	int Load(std::string fileName);

	//ファイルを読み込む関数
	//引数：handle		ファイル番号, カラー
	//戻値：なし
	void Draw(int handle, D3DXCOLOR color = D3DXCOLOR(1, 1, 1, 1));

	//行列をセットする関数
	//引数：handle　ファイル番号、matrix　行列
	//戻値：なし
	void SetMatrix(int handle, D3DXMATRIX& matrix);

	//解放処理
	//引数：handle　ファイル番号
	//戻値：なし
	void Release(int handle);

	//ゲーム終了時に呼び出される解放処理
	//引数：なし
	//戻値：なし
	void AllRelease();
	
	//画像の情報を返す
	//引数：hPict 画像番号
	D3DXIMAGE_INFO GetImageInfo(int hPict);
};


