#include <Windows.h>
#include <mmsystem.h>
#include "Global.h"
#include "Direct3D.h"
#include "Sprite.h"
#include "Fbx.h"
#include "Input.h"
#include "RootJob.h"
#include "Model.h"
#include "Image.h"
#include "../resource.h"
#include "../PlayScene.h"
#include "../Controller.h"
#include "../Map.h"
#include "../FileOperation.h"
#pragma comment (lib, "winmm.lib") 

#ifdef _DEBUG
#include <crtdbg.h>
#endif

//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//メッセージループを続けるか
//PostQuitMessage(0)だけではメッセージループが終わらない場合があった
bool isMessageLoop = true;

const char* WIN_CLASS_NAME = "MapEditor";
//ディスプレイのサイズを取得してウィンドウの大きさに設定する
const int WINDOW_WIDTH = GetSystemMetrics(SM_CXSCREEN);
const int WINDOW_HEIGHT = GetSystemMetrics(SM_CYSCREEN);

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);             //この構造体のサイズ
	wc.hInstance = hInstance;                   //インスタンスハンドルを指定
	wc.lpszClassName = WIN_CLASS_NAME;            //ウィンドウクラス名(設計図の名前)
	wc.lpfnWndProc = WndProc;                   //ウィンドウプロシージャ(プロトタイプ宣言にある)
	wc.style = CS_VREDRAW | CS_HREDRAW;         //スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION); //アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);   //小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);   //マウスカーソル
	wc.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);//メニュー
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); //背景（白）
	RegisterClassEx(&wc); //クラスを登録

	RECT winRect = { 0,0,WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, TRUE);

	//ウィンドウを作成
	HWND hWnd = CreateWindow(//HWNDはウィンドウハンドル(ウィンドウの番号)
		WIN_CLASS_NAME,        //ウィンドウクラス名
		WIN_CLASS_NAME,     //タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW, //スタイル（普通のウィンドウ）ポップアップにできたりする
		CW_USEDEFAULT,       //表示位置左（おまかせ）
		CW_USEDEFAULT,       //表示位置上（おまかせ）
		winRect.right - winRect.left,                 //ウィンドウ幅
		winRect.bottom - winRect.top,                 //ウィンドウ高さ
		NULL,                //親ウインドウ（なし）
		NULL,                //メニュー（なし）
		hInstance,           //インスタンスハンドルが入る
		NULL                 //パラメータ（なし）
	);

	assert(hWnd != NULL);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	WindowSize windowSize;
	windowSize.width = WINDOW_WIDTH;
	windowSize.height = WINDOW_HEIGHT;
	Global::SetWindowSize(windowSize);

	RootJob* pRootJob;

	//初期化
	Direct3D::Initialize(hWnd);

	//DirectInputの初期化
	Input::Initialize(hWnd);

	pRootJob = new RootJob;
	pRootJob->Initialize();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT && isMessageLoop)
	{	
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		//メッセージなし
		else
		{
			timeBeginPeriod(1);
			int nowTime = timeGetTime();
			static int lastDispTime = nowTime;
			static int lastUpdateTime = nowTime;
			static int fps = 0;

			if (nowTime - lastDispTime > 1000)
			{
				//FPS表示
				//char str[16];
				//wsprintf(str, "FPS=%d", fps);
				//SetWindowText(hWnd, str);
				//fps = 0;
				//lastDispTime = nowTime;
			}


			if ((nowTime - lastUpdateTime) * 60 >= 1000.0f)
			{
				fps++;
				lastUpdateTime = nowTime;
				//ゲームの処理

				//入力情報の更新
				Input::Update();
				pRootJob->UpdateSub();

				Direct3D::BeginDraw();

				pRootJob->DrawSub();

				Direct3D::EndDraw();

				timeEndPeriod(1);
			}
		}
	}

	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Input::Release();
	Direct3D::Release();
	Model::AllRelease();
	Image::AllRelease();

	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0); //プログラム終了
		isMessageLoop = false;
		return 0;

		//マウスホイールを操作した
	case WM_MOUSEWHEEL:
	{
		//マウスのホイールのノッチ数を求める
		int notch = GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA;

		//ノッチ数に応じて選択しているブロックを変える
		//まだコントローラーが生成されていなければnullが入る
		IGameObject* pController = (SceneManager::GetCurrentScene())->FindChildObject("Controller");
		if (pController != nullptr)
		{
			//今選んでいるブロックはenumで管理しているため値が範囲外にならないようにする
			BLOCK_ENUM selectBlock = ((Controller*)pController)->GetSelectBlock();
			if ((notch + selectBlock) >= 0 && (notch + selectBlock) < BLOCK_MAX)
			{
				((Controller*)pController)->SetSelectBlock((BLOCK_ENUM)(selectBlock + notch));
			}
		}
	}
	return 0;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_MENU_SAVE:
			FileOperation::NewSave();
			return 0;

		case ID_MENU_OVER_WRITING_SAVE:
			FileOperation::OverwritingSave();
			return 0;

		case ID_MENU_OPEN:
			FileOperation::Load();
			return 0;
		}
		return 0;

	case WM_ENTERMENULOOP:
		while (ShowCursor(true) < 0);
		return 0;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}