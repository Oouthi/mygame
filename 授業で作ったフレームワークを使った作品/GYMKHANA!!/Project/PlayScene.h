#pragma once
#include <iostream>
#include <fstream>
#include "Engine/global.h"

//メンバ変数のhCountDownPict_の配列の要素数をマジックナンバーにしないための変数
const int NUMBER_OF_COUNTDOWN = 4;

class Car;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	//インスタンスのポインタを指す変数
	Car* pCar_;

	//ファイル入出力用
	std::fstream bestTimeFile_;

	//カウントダウンの画像番号
	int hCountDownPict_[NUMBER_OF_COUNTDOWN];

	//数字画像の画像番号
	int hNumberPict_[10];

	//コロン画像の画像番号
	int hColonRPict_;

	//スラッシュ画像番号
	int hSlashPict_;

	//time文字画像番号
	int hTimePict_;

	//bestTime文字画像番号
	int hBestTimePict_;

	//laps文字画像番号
	int hLapsPict_;

	//finish文字画像番号
	int hFinishPict_;

	//ベストタイムが出た時のfinish文字画像
	int hNewRecordFinishPict_;

	//車がチェックポイントの判定に正面から触れたかどうか
	bool checkpointFlag_;

	//車がチェックポイントを通ったかどうか
	bool checkpointPassingFlag_;

	//車がゴールの判定に触れたかどうか
	bool goalFlag_;

	//規定回数周回が終わったかどうか
	bool finishFlag_;

	//カウントダウンが終わったかどうか
	bool countDownFinishFlag_;

	//ベストタイムを記録するとき用のフラグ
	bool saveFlag_;

	//時間計測を始めるとき用のフラグ
	bool startMeasurementFlag_;

	//今何週目か
	int laps_;

	//時間測定用変数
	DWORD countDownStartTime_;
	DWORD countDownNowTime_;
	DWORD startTime_;
	DWORD nowTime_;

	//ベストタイムを入れる変数
	double bestTime_;

public:
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //更新
  void Update() override;

  //描画
  void Draw() override;

  //開放
  void Release() override;

  //衝突
  void OnCollision(IGameObject *pTarget) override;

  void SetCheckPointFlag(bool checkPointFlag)
  {
	  checkpointFlag_ = checkPointFlag;
  }

  void SetGoalFlag(bool goalFlag)
  {
	  goalFlag_ = goalFlag;
  }

  void SetCheckpointPassingFlag(bool checkpointPassingFlag)
  {
	  checkpointPassingFlag_ = checkpointPassingFlag;
  }
};