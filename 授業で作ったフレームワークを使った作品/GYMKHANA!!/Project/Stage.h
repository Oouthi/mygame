#pragma once
#include <string>
#include "Engine/IGameObject.h"

const std::string GOAL_COLLIDER_NAME = "Goal";
const std::string CHECKPOINT_COLLIDER_NAME = "Checkpoint";

//ステージを管理するクラス
class Stage : public IGameObject
{
	//HLTLで書いた処理がここに入る
	LPD3DXEFFECT pEffect_;

	//ゴールのモデル番号
	int hGoalModel_;

	//ステージのモデル番号
	int hStageModel_;

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

private:
	//コース作成
	void MakeCourse();
};