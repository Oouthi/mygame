#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include "Engine/Image.h"
#include "Engine/Model.h"
#include "Engine/SceneManager.h"
#include "Engine/Direct3D.h"
#include "PlayScene.h"
#include "Car.h"
#include "StraightRoad.h"
#include "CurvRoad.h"
#include "Stage.h"

//周回する回数
const int MAX_NUMBER_OF_RAPS = 2;

//素材のパス
const std::string NUMBER_PICT_PATH[] = { "data/NumberPict/zero.png", 
										  "data/NumberPict/one.png",
										  "data/NumberPict/two.png", 
										  "data/NumberPict/three.png", 
										  "data/NumberPict/four.png", 
										  "data/NumberPict/five.png", 
										  "data/NumberPict/six.png", 
										  "data/NumberPict/seven.png",
										  "data/NumberPict/eight.png",
										  "data/NumberPict/nine.png", };
const std::string COUNTDOWN_NUMBER_PICT_PATH[] = { "data/CountDownPict/three.png",
													"data/CountDownPict/two.png",
													"data/CountDownPict/one.png",
													"data/CountDownPict/start.png", };
const std::string COLON_PICT_PATH = "data/colon.png";
const std::string TIME_PICT_PATH = "data/time.png";
const std::string BEST_TIME_PICT_PATH = "data/bestTime.png";
const std::string LAPS_PICT_PATH = "data/laps.png";
const std::string SLASH_PICT_PATH = "data/slash.png";
const std::string FINISH_PICT_PATH = "data/finish.png";
const std::string NEW_RECORD_FINISH_PICT_PATH = "data/NewRecordFinish.png";
const std::string BEST_TIME_FILE_PATH = "data/bestTime.txt";

//画像の位置をずらすために使う値
const int IMAGE_SIZE_Y = 50;
const int IMAGE_SIZE_X = 40;
const int TIME_NUMBER_INIT_POS_X = -100;

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"),	//親のコンストラクタを引数ありで呼ぶ
	hSlashPict_(-1), hColonRPict_(-1), hTimePict_(-1), hBestTimePict_(-1), hLapsPict_(-1), hFinishPict_(-1), hNewRecordFinishPict_(-1),
	countDownStartTime_(0), countDownNowTime_(0), startTime_(0), nowTime_(0), checkpointFlag_(false), goalFlag_(false), countDownFinishFlag_(false),
	checkpointPassingFlag_(false), laps_(1), finishFlag_(false), bestTime_(0), pCar_(nullptr), saveFlag_(true), startMeasurementFlag_(true)
{
}

//初期化
void PlayScene::Initialize()
{
	CreateGameObject<Stage>(this);
	pCar_ = CreateGameObject<Car>(this);
	//カウントダウンしている間は動かせないようにする
	pCar_->SetIsUpdate(false);

	//数字画像のロード
	for (int i = 0; i < 10; i++)
	{
		hNumberPict_[i] = Image::Load(NUMBER_PICT_PATH[i]);
		assert(hNumberPict_[i] >= 0);
	}

	//カウントダウン用数字画像ロード
	for (int i = 0; i < NUMBER_OF_COUNTDOWN; i++)
	{
		hCountDownPict_[i] = Image::Load(COUNTDOWN_NUMBER_PICT_PATH[i]);
		assert(hCountDownPict_[i] >= 0);
	}

	//コロン画像のロード
	hColonRPict_ = Image::Load(COLON_PICT_PATH);

	//time文字画像のロード
	hTimePict_ = Image::Load(TIME_PICT_PATH);

	//beatTime文字画像のロード
	hBestTimePict_ = Image::Load(BEST_TIME_PICT_PATH);

	//laps文字画像のロード
	hLapsPict_ = Image::Load(LAPS_PICT_PATH);

	//スラッシュ文字画像ロード
	hSlashPict_ = Image::Load(SLASH_PICT_PATH);

	//finish文字画像ロード
	hFinishPict_ = Image::Load(FINISH_PICT_PATH);

	//NewRecordFinish文字画像ロード
	hNewRecordFinishPict_ = Image::Load(NEW_RECORD_FINISH_PICT_PATH);


	//ベストタイム読み込み
	bestTimeFile_.open(BEST_TIME_FILE_PATH, std::ios_base::in);
	std::string bestTimeString;
	getline(bestTimeFile_, bestTimeString);
	bestTime_ = atoi(bestTimeString.c_str());
	bestTimeFile_.close();

	//時間計測開始
	countDownStartTime_ = GetTickCount();
}

//更新
void PlayScene::Update()
{
	//時間を計測してからNUMBER_OF_COUNTDOWN秒時間が経過したらカウントダウン終了
	countDownNowTime_ = GetTickCount();
	if (startMeasurementFlag_ && (countDownNowTime_ - countDownStartTime_) / 1000 >= NUMBER_OF_COUNTDOWN)
	{
		startMeasurementFlag_ = false;
		countDownFinishFlag_ = true;
		pCar_->SetIsUpdate(true);
		startTime_ = GetTickCount();
	}

	if (countDownFinishFlag_ && !finishFlag_)
	{
		nowTime_ = GetTickCount();
	}

	//finishフラグが立った後の処理
	if (finishFlag_)
	{
		if (saveFlag_)
		{
			saveFlag_ = false;
			//ベストタイムよりも今回のタイムのほうが早ければ更新する
			if (bestTime_ > (nowTime_ - startTime_))
			{
				bestTimeFile_.open(BEST_TIME_FILE_PATH, std::ios_base::out);
				bestTimeFile_ << std::to_string(nowTime_ - startTime_);
				bestTimeFile_.close();
			}
		}

		//エンターを押したらタイトルに戻る
		if (Input::IsKey(DIK_RETURN))
		{
			((SceneManager*)pParent_)->ChangeScene(SCENE_ID_TITLE);
		}
	}

	//ゴールするための一連の処理
	//ゴールするにはcheckpointPassingFlag_を立てた状態でゴールに正面から触れなければいけない。
	//checkpointPassingFlag_はチェックポイントとゴールに同時に触れていると立ち、
	//ゴールするか逆走した状態でチェックポイントに触れることで折れる。
	//同時に触れなければいけないためチェックポイントはゴールのすぐ前に設定してある。
	//同時に触れなければいけない理由としては
	//ゴールからチェックポイントが離れていると逆走してチェックポイントに触れてから
	//従走に戻りゴール判定に触れることで1週きちんと回らなくてもゴールできてしまうから。
	//これだけだとチェックポイントとゴールに同時に触れた瞬間
	//checkpointPassingFlag_が立ち次のフレームですぐゴールした判定になってしまう。
	//そこでチェックポイントがゴールの前の位置に設定してあることを利用し
	//ゴールの条件にチェックポイントには触れていないことを加えている。
	if (checkpointFlag_ && goalFlag_)
	{
		checkpointPassingFlag_ = true;
	}
	if (goalFlag_ && checkpointPassingFlag_ && !checkpointFlag_)
	{
		if (laps_ < MAX_NUMBER_OF_RAPS)
		{
			laps_++;
		}
		else
		{
			finishFlag_ = true;
		}
		checkpointPassingFlag_ = false;
	}
}

//描画
void PlayScene::Draw()
{
	//画像表示
	//画像を表示する位置に使う変数
	D3DXMATRIX m;
	D3DXMatrixIdentity(&m);

	int imageY = -100;
	int imageX = 0;

	enum DIGIT
	{
		ONE = 0,
		TEN,
		DIGIT_MAX
	};

	//n桁目の数字を入れる配列
	int secondsDigitArray[DIGIT_MAX];
	int minutesDigitArray[DIGIT_MAX];

	enum TIME_TYPE
	{
		BEST_TIME = 0,
		CURRENTLY_TIME,
		TIME_MAX
	};

	//現在計っている時間とベストタイムを入れる配列
	//for文の中で使う
	unsigned int elapsedSecondsArray[TIME_MAX];

	elapsedSecondsArray[BEST_TIME] = bestTime_ / 1000;
	elapsedSecondsArray[CURRENTLY_TIME] = (unsigned int)(nowTime_ - startTime_) / 1000;

	//bestTimeとtimeの文字番号も配列に入れる
	int timeTypePictArray[TIME_MAX];
	timeTypePictArray[BEST_TIME] = hBestTimePict_;
	timeTypePictArray[CURRENTLY_TIME] = hTimePict_;

	for (int timeType = BEST_TIME; timeType < TIME_MAX; timeType++)
	{
		imageX = -50;

		//bestTimeまたはtimeの文字表示
		D3DXMatrixTranslation(&m, imageX, imageY, 0);
		Image::SetMatrix(timeTypePictArray[timeType], m);
		Image::Draw(timeTypePictArray[timeType]);
		imageY += IMAGE_SIZE_Y;

		if (elapsedSecondsArray[timeType] < 3600)
		{
			imageX = TIME_NUMBER_INIT_POS_X;

			//分表示
			int minutes = elapsedSecondsArray[timeType] / 60;

			//10の位から表示する
			for (int i = TEN; i >= ONE; i--)
			{
				minutesDigitArray[i] = minutes / pow(10, i);
				minutes -= minutesDigitArray[i] * pow(10, i);

				D3DXMatrixTranslation(&m, imageX, imageY, 0);
				Image::SetMatrix(hNumberPict_[minutesDigitArray[i]], m);
				Image::Draw(hNumberPict_[minutesDigitArray[i]]);
				imageX += IMAGE_SIZE_X;

			}

			//コロン表示
			D3DXMatrixTranslation(&m, imageX, imageY, 0);
			Image::SetMatrix(hColonRPict_, m);
			Image::Draw(hColonRPict_);
			imageX += IMAGE_SIZE_X;

			//秒表示
			int seconds = elapsedSecondsArray[timeType] % 60;
			//10の位から表示する
			for (int i = TEN; i >= ONE; i--)
			{
				secondsDigitArray[i] = seconds / pow(10, i);
				seconds -= secondsDigitArray[i] * pow(10, i);

				D3DXMatrixTranslation(&m, imageX, imageY, 0);
				Image::SetMatrix(hNumberPict_[secondsDigitArray[i]], m);
				Image::Draw(hNumberPict_[secondsDigitArray[i]]);
				imageX += IMAGE_SIZE_X;
			}
		}
		//3600秒以上の場合59:59と表示する
		else
		{
			imageX = TIME_NUMBER_INIT_POS_X;

			//分表示
			D3DXMatrixTranslation(&m, imageX, imageY, 0);
			Image::SetMatrix(hNumberPict_[5], m);
			Image::Draw(hNumberPict_[5]);
			imageX += IMAGE_SIZE_X;

			D3DXMatrixTranslation(&m, imageX, imageY, 0);
			Image::SetMatrix(hNumberPict_[9], m);
			Image::Draw(hNumberPict_[9]);
			imageX += IMAGE_SIZE_X;

			//コロン表示
			D3DXMatrixTranslation(&m, imageX, imageY, 0);
			Image::SetMatrix(hColonRPict_, m);
			Image::Draw(hColonRPict_);
			imageX += IMAGE_SIZE_X;

			//秒表示
			D3DXMatrixTranslation(&m, imageX, imageY, 0);
			Image::SetMatrix(hNumberPict_[5], m);
			Image::Draw(hNumberPict_[5]);
			imageX += IMAGE_SIZE_X;

			D3DXMatrixTranslation(&m, imageX, imageY, 0);
			Image::SetMatrix(hNumberPict_[9], m);
			Image::Draw(hNumberPict_[9]);
		}

		imageY += IMAGE_SIZE_Y;
	}

	//ラップ文字表示
	imageX = -50;
	D3DXMatrixTranslation(&m, imageX, imageY, 0);
	Image::SetMatrix(hLapsPict_, m);
	Image::Draw(hLapsPict_);
	imageY += IMAGE_SIZE_Y;

	//現在の周回数を表示
	imageX = TIME_NUMBER_INIT_POS_X;

	D3DXMatrixTranslation(&m, imageX, imageY, 0);
	Image::SetMatrix(hNumberPict_[laps_], m);
	Image::Draw(hNumberPict_[laps_]);
	imageX += IMAGE_SIZE_X;

	//スラッシュ表示
	D3DXMatrixTranslation(&m, imageX, imageY, 0);
	Image::SetMatrix(hSlashPict_, m);
	Image::Draw(hSlashPict_);
	imageX += IMAGE_SIZE_X;

	//ゴールするのに必要な周回数を表示
	D3DXMatrixTranslation(&m, imageX, imageY, 0);
	Image::SetMatrix(hNumberPict_[MAX_NUMBER_OF_RAPS], m);
	Image::Draw(hNumberPict_[MAX_NUMBER_OF_RAPS]);
	imageX += IMAGE_SIZE_X;

	//フィニッシュ画像表示
	if (finishFlag_)
	{
		//ベストタイムが出たかどうか
		if ((nowTime_ - startTime_) < bestTime_)
		{
			Image::SetMatrix(hNewRecordFinishPict_, worldMatrix_);
			Image::Draw(hNewRecordFinishPict_);
		}
		else
		{
			Image::SetMatrix(hFinishPict_, worldMatrix_);
			Image::Draw(hFinishPict_);
		}
	}

	//カウントダウン表示
	if (!countDownFinishFlag_)
	{
		int CountDownPictNumber = (countDownNowTime_ - countDownStartTime_) / 1000;
		if (CountDownPictNumber >= NUMBER_OF_COUNTDOWN)
		{
			CountDownPictNumber = NUMBER_OF_COUNTDOWN - 1;
		}

		Image::SetMatrix(hCountDownPict_[CountDownPictNumber], worldMatrix_);
		Image::Draw(hCountDownPict_[CountDownPictNumber]);
	}
}

//開放
void PlayScene::Release()
{
}

void PlayScene::OnCollision(IGameObject * pTarget)
{
}
