#include <cmath>
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Engine/Direct3D.h"
#include "Stage.h"
#include "CurvRoad.h"
#include "StraightRoad.h"
#include "RoadParts.h"

//パーツのサイズをまとめた配列
//中心から端までのサイズをパーツを置く前後でroadPositionに足すため道路パーツの半分の大きさを設定しておく
const D3DXVECTOR3 RoadSize[] = { D3DXVECTOR3(0, 0, STRAIGHT_ROAD_LENGTH / 2) , D3DXVECTOR3(0, 0, CURVE_ROAD_LENGTH / 2) };

//素材のパス
const std::string GOAL_MODEL_PATH = "Data/Model/goal.fbx";
const std::string STAGE_MODEL_PATH = "Data/Model/stage.fbx";

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), hGoalModel_(-1), hStageModel_(-1), pEffect_(nullptr)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//普通のシェーダを使う
	//HLSLで書いたシェーダーをロード
	//第一引数：デバイスオブジェクト
	//第二引数：使いたいシェーダーのファイル名(パス)
	//第七引数：格納先のポインタ
	//エラーを入れる変数
	LPD3DXBUFFER err = 0;
	if (D3DXCreateEffectFromFile(Direct3D::pDevice,
		"HLSL/CommonShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_, &err))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}

	assert(pEffect_ != nullptr);

	//ゴールモデルのロード
	hGoalModel_ = Model::Load(GOAL_MODEL_PATH);

	//ステージモデルのロード
	hStageModel_ = Model::Load(STAGE_MODEL_PATH);

	//コース生成
	MakeCourse();

	//ゴール用のコライダー作成
	BoxCollider* goal = new BoxCollider(GOAL_COLLIDER_NAME, D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(STRAIGHT_ROAD_LENGTH / 2, 1, 1), this);
	AddCollider(goal);

	BoxCollider* checkpoint = new BoxCollider(CHECKPOINT_COLLIDER_NAME, D3DXVECTOR3(0, 0, 3), D3DXVECTOR3(STRAIGHT_ROAD_LENGTH / 2, 1, 1), this);
	AddCollider(checkpoint);
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	//3Dモデル表示
	Model::SetMatrix(hGoalModel_, worldMatrix_);
	Model::Draw(hGoalModel_);

	Model::SetMatrix(hStageModel_, worldMatrix_);
	Model::Draw(hStageModel_);
}

//開放
void Stage::Release()
{
	SAFE_RELEASE(pEffect_);
}

void Stage::MakeCourse()
{
	enum DIR
	{
		LEFT = -1,
		FRONT = 0,
		RIGHT = 1,
	};

	DIR coursePartsArray[] = { FRONT, FRONT, FRONT, FRONT, FRONT, FRONT, FRONT, LEFT, RIGHT, LEFT, FRONT,FRONT, FRONT, FRONT, LEFT,
							   RIGHT, LEFT, FRONT, FRONT, LEFT, RIGHT, LEFT, FRONT , FRONT, FRONT, RIGHT, LEFT, RIGHT, FRONT, FRONT, FRONT, FRONT,
							   FRONT, RIGHT, LEFT , RIGHT, FRONT, FRONT, FRONT, RIGHT, LEFT, RIGHT, FRONT, FRONT, FRONT, FRONT, LEFT, RIGHT, LEFT,
							   FRONT, LEFT, RIGHT, LEFT, FRONT, FRONT, FRONT, FRONT, FRONT, FRONT, FRONT, FRONT, FRONT, LEFT, RIGHT, LEFT, FRONT,
							   FRONT, FRONT, FRONT, FRONT, LEFT, RIGHT, LEFT, RIGHT, LEFT, RIGHT, LEFT, RIGHT, LEFT, FRONT, FRONT, FRONT, FRONT, FRONT};

	//コースのパーツの位置
	D3DXVECTOR3 roadPosition = D3DXVECTOR3(0, 0, 0);
	
	//どちらを使うかを判別するフラグ的なenum
	enum ROAD_PARTS_TO_USE
	{
		STRAIGHT = 0,
		CURVE_ROAD
	}roadPartsToUseFlag;

	//y軸回転用の変数
	int rotateY = 0;
	D3DXMATRIX mY;
	D3DXMatrixIdentity(&mY);

	//回転させたポジションを入れる変数
	D3DXVECTOR3 addPosition = D3DXVECTOR3(0, 0, 0);

	//パーツの位置を変えるためのポインタ
	RoadParts* pRoadParts;

	for (int i = 0; i < (sizeof(coursePartsArray) / sizeof(DIR)); i++)
	{
		//道路パーツ生成
		if (coursePartsArray[i] == FRONT)
		{
			pRoadParts = CreateGameObject<StraightRoad>(this);
			pRoadParts->SetEffect(pEffect_);
			roadPartsToUseFlag = STRAIGHT;
		}
		else
		{
			pRoadParts = CreateGameObject<CurvRoad>(this);
			pRoadParts->SetEffect(pEffect_);
			roadPartsToUseFlag = CURVE_ROAD;
		}

		//パーツの位置移動
		//回転させる
		D3DXMatrixRotationY(&mY, D3DXToRadian(rotateY));
		D3DXVec3TransformCoord(&addPosition, &RoadSize[roadPartsToUseFlag], &mY);

		//道路のパーツを動かす
		roadPosition += addPosition;
		pRoadParts->SetPosition(roadPosition);

		//曲がる道路の基本モデルが右に曲がる形になっているので左に曲がるときは90度回転を足す
		if (coursePartsArray[i] == LEFT)
		{
			pRoadParts->SetRotate(D3DXVECTOR3(0, rotateY + 90, 0));
		}
		else
		{
			pRoadParts->SetRotate(D3DXVECTOR3(0, rotateY, 0));
		}

		

		//道が右に曲がったのでrotateYを足して次のパーツが道に沿って回転するようにする
		rotateY += (90 * coursePartsArray[i]);

		//次に置くパーツのために回転させてaddPositionをroadPositionに足す
		D3DXMatrixRotationY(&mY, D3DXToRadian(rotateY));
		D3DXVec3TransformCoord(&addPosition, &RoadSize[roadPartsToUseFlag], &mY);

		roadPosition += addPosition;
	}
}
