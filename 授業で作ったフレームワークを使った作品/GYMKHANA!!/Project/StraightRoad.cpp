#include "StraightRoad.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Engine/Direct3D.h"

//素材のパス
std::string STRAIGHT_ROAD_MODEL_PATH = "Data/Model/straightRoad.fbx";

//コンストラクタ
StraightRoad::StraightRoad(IGameObject * parent)
	:RoadParts(parent, "StraightRoad"), hModel_(-1)
{
}

//デストラクタ
StraightRoad::~StraightRoad()
{
}

//初期化
void StraightRoad::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load(STRAIGHT_ROAD_MODEL_PATH);
	assert(hModel_ >= 0);

	//コライダー作成
	//モデルの左右の壁に合わせて2つ作る
	//右側
	BoxCollider* wall = new BoxCollider(STRIGHT_ROAD_RIGHT_WALL_COLLIDER_NAME, D3DXVECTOR3((STRAIGHT_ROAD_WIGHT - WALL_THICKNESS) / 2, WALL_HEIGHT / 2, 0), D3DXVECTOR3(WALL_THICKNESS, WALL_HEIGHT, STRAIGHT_ROAD_LENGTH), this);
	AddCollider(wall);
	//左側
	wall = new BoxCollider(STRIGHT_ROAD_LEFT_WALL_COLLIDER_NAME, D3DXVECTOR3((-STRAIGHT_ROAD_WIGHT + WALL_THICKNESS) / 2, WALL_HEIGHT / 2, 0), D3DXVECTOR3(WALL_THICKNESS, WALL_HEIGHT, STRAIGHT_ROAD_LENGTH), this);
	AddCollider(wall);
}

//更新
void StraightRoad::Update()
{
}

//描画
void StraightRoad::Draw()
{
	//プロジェクション行列
	D3DXMATRIX proj;
	//最後に設定したプロジェクション行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ビュー行列
	D3DXMATRIX view;
	//最後に設定したビュー行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//シェーダーに渡すための行列を合成
	//ワールド x ビュー x プロジェクション
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//シェーダー側に合成した行列をセット
	//第1引数：HLSLのグローバル変数名
	//第2引数：オブジェクト側から渡す行列
	pEffect_->SetMatrix("WVP", &matWVP);

	//いったんワールド行列をコピー
	D3DXMATRIX mat = worldMatrix_;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	//面が横に伸びたら法線は縦向きに
	//面が縦に伸びたら法線は横向きにしたい
	//拡大縮小行列は逆行列にしたい
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//回転行列と拡大縮小行列の逆行列を合成してシェーダーに渡す
	//※拡大縮小行列の逆行列をかけたので、このベクトルの長さは1じゃない
	mat = scale * rotateZ * rotateX * rotateY;
	pEffect_->SetMatrix("RS", &mat);

	//ライトの方向を決めてるベクトルをDirect3Dから取ってくる
	D3DLIGHT9 lightState;
	Direct3D::pDevice->GetLight(0, &lightState);
	//ライトの方向を決めてるベクトルを渡す
	//ライト->物体へのベクトル
	//※LambertShaderは物体->ライトのベクトルを使うので反転させる必要あり
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);

	//BeginからEndまでの範囲にシェーダーを適用する
	pEffect_->Begin(NULL, 0);
	pEffect_->BeginPass(0);

	Model::SetMatrix(hModel_, worldMatrix_);
	Model::SetEffect(hModel_, pEffect_);
	Model::Draw(hModel_);

	pEffect_->EndPass();
	pEffect_->End();
}

//開放
void StraightRoad::Release()
{
}