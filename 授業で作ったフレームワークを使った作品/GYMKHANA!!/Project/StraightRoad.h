#pragma once
#include "Engine/IGameObject.h"
#include "RoadParts.h"

const float STRAIGHT_ROAD_LENGTH = 30.f;
const float STRAIGHT_ROAD_WIGHT = 17.f;
const std::string STRIGHT_ROAD_RIGHT_WALL_COLLIDER_NAME = "StringRoadRightWall";
const std::string STRIGHT_ROAD_LEFT_WALL_COLLIDER_NAME = "StringRoadLeftWall";

//直線の道路を管理するクラス
class StraightRoad : public RoadParts
{
	int hModel_;    //モデル番号

public:
	//コンストラクタ
	StraightRoad(IGameObject* parent);

	//デストラクタ
	~StraightRoad();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};