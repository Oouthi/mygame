#pragma once
#include <string>
#include "Engine/IGameObject.h"

//壁の厚さ
const float WALL_THICKNESS = 1;
//壁の高さ
const float WALL_HEIGHT = 3;

//◆◆◆を管理するクラス
class RoadParts : public IGameObject
{

protected:
	//HLTLで書いた処理がここに入る
	LPD3DXEFFECT pEffect_;

public:
	//コンストラクタ
	RoadParts(IGameObject* parent);
	RoadParts(IGameObject* parent, std::string name);

	//デストラクタ
	~RoadParts();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetEffect(LPD3DXEFFECT pEffect)
	{
		pEffect_ = pEffect;
	}

};