#include "Direct3D.h"

LPDIRECT3D9 Direct3D::pD3d = nullptr;
LPDIRECT3DDEVICE9 Direct3D::pDevice = nullptr;

void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);

	assert(pD3d != nullptr);

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;                 //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));           //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;

	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);

	assert(pDevice != nullptr);

	//アルファブレンド
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	//ライトを設置
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));
	lightState.Type = D3DLIGHT_DIRECTIONAL;			//光源の種類
	lightState.Direction = D3DXVECTOR3(1, -1, 1);	//方向
	lightState.Diffuse.r = 1.0f;					//色の設定0~1を入れる
	lightState.Diffuse.g = 1.0f;
	lightState.Diffuse.b = 1.0f;

	pDevice->SetLight(0, &lightState);
	pDevice->LightEnable(0, TRUE);


	//カメラ
	D3DXMATRIX view, proj;	//ビュー行列とプロジェクション行列を作るための変数
	//ビュー行列を作るための関数 (カメラの情報を保存する変数, &カメラの位置,どの「位置」見るか,カメラそのものを傾ける
	//D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 500, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
	//プロジェクション行列を作るための行列 (行列を保存するための変数,画角,アスペクト比ウィンドウの比率,描画し始める位置を面にしないといけないため少し離す,どこまで表示するか)
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 1000.0f);
	//どの座標として使うかを指定
	pDevice->SetTransform(D3DTS_VIEW, &view);
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

void Direct3D::BeginDraw()
{
	//画面をクリア
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);

	//描画開始
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();

	//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);
}

void Direct3D::Release()
{
	//開放処理
	//生成したオブジェクトと逆に開放
	//ReleaseはpD開放専用関数
	pDevice->Release();
	pD3d->Release();
}
