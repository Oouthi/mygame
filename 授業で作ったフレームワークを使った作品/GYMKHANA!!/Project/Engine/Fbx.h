#pragma once
#include "Global.h"
#include <fbxsdk.h>

#pragma comment(lib,"libfbxsdk-mt.lib")

class Fbx
{
	//(クラス名、コンストラクタ名、デストラクタ名が違うだけで、あとはPolygonクラスと全く同じ)
	struct Vertex
	{
		D3DXVECTOR3 pos;	//位置
		D3DXVECTOR3 normal;	//法線(面の表)
		D3DXVECTOR2 uv;		//貼り付けるテクスチャの座標
	};

	LPD3DXEFFECT pEffect_;

	FbxManager*  pManager_;
	FbxImporter* pImporter_;
	FbxScene*    pScene_;
	int vertexCount_;
	int polygonCount_;
	int indexCount_;
	int materialCount_;
	int* polygonCountOfMaterial_;

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9* ppIndexBuffer_;
	LPDIRECT3DTEXTURE9* pTexture_; //テクスチャ
	D3DMATERIAL9*      pMaterial_;

	void CheckNode(FbxNode* pNode);
	void CheckMesh(FbxMesh* pMesh);

public:
	Fbx(LPD3DXEFFECT pEffect);
	~Fbx();

	void Load(const char* imagePass);
	void Draw(const D3DXMATRIX &matrix);

	void SetEffect(LPD3DXEFFECT pEffect)
	{
		pEffect_ = pEffect;
	}

};

