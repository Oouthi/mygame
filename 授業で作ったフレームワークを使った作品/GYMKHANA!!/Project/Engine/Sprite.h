#pragma once
#include "Global.h"

//スプライトとテクスチャを作らないと画像表示できない
//スプライトという板を用意して、テクスチャで色を付ける！
class Sprite
{
	LPD3DXSPRITE		pSprite_;	//スプライトは板
	LPDIRECT3DTEXTURE9	pTexture_;	//テクスチャは色

	struct ImageSize
	{
		int width;
		int height;
	}imageSize_;


public:
	Sprite();
	~Sprite();

	//スプライト・テクスチャを作成する
	//引数：FileName	画像入れ
	//戻値：なし
	void Load(const char* FileName);

	//行列を作成し描画する
	//引数：matrix	行列, カラー
	//戻値：なし
	void Draw(D3DXMATRIX& matrix, D3DXCOLOR color);	//描写処理
	//画像の幅を返す
	//戻値；int 画像の幅
	int GetImageSizeWidth()
	{
		return imageSize_.width;
	}

	//画像の高さを返す
	//戻値；int 画像の高さ
	int GetImageSizeHeight()
	{
		return imageSize_.height;
	}
};

