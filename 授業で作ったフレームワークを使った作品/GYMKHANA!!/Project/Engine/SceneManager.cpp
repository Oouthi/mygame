#include "SceneManager.h"
#include "../PlayScene.h"
#include "../TitleScene.h"

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_TITLE;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_TITLE;
IGameObject* SceneManager::pCurrentSecne_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
	currentSceneID_ = SCENE_ID_TITLE;
	nextSceneID_ = SCENE_ID_TITLE;
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentSecne_ = CreateGameObject<TitleScene>(this);
}

//更新
void SceneManager::Update()
{

	if (currentSceneID_ != nextSceneID_)
	{
		auto scene = childList_.begin();
		{
			(*scene)->ReleaseSub();
			SAFE_DELETE(*scene);
			childList_.clear();		//リストの開放

			switch (nextSceneID_)
			{
			case SCENE_ID_TITLE:
				pCurrentSecne_ = CreateGameObject<TitleScene>(this);
				break;

			case SCENE_ID_PLAY:
				pCurrentSecne_ = CreateGameObject<PlayScene>(this);
				break;
			}
			//childList_.erase(scene);
			currentSceneID_ = nextSceneID_;
		}
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
